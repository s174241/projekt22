#Author: Adam
Feature: Getting users from projects or likewise
	Description: Different tests regarding getting specific users from a list
	Actors: Developer

Scenario: Getting list of developers working on project
	Given A project exists
	And The user is a project leader
	And The project has an activity with name "the activity"
	And A developer "dev1" exists
	And A developer "dev2" exists
	And The user adds the developer "dev1" to the activity "the activity"
	And The user adds the developer "dev2" to the activity "the activity"
	Then The list of developers on the first project contains (at least) "dev1" and "dev2"

Scenario: Getting list of developers working on project, when devs are working on different activities
	Given A project exists
	And The user is a project leader
	And The project has an activity with name "the activity"
	And The project has an activity with name "the second activity"
	And A developer "dev1" exists
	And A developer "dev2" exists
	And The user adds the developer "dev1" to the activity "the activity"
	And The user adds the developer "dev2" to the activity "the second activity"
	Then The list of developers on the first project contains (at least) "dev1" and "dev2"

Scenario: Other developers are allowed on the list of workers on project
	Given A project exists
	And The user is a project leader
	And The project has an activity with name "the activity"
	And A developer "dev1" exists
	And A developer "dev2" exists
	And A developer "dev3" exists
	And The user adds the developer "dev1" to the activity "the activity"
	And The user adds the developer "dev2" to the activity "the activity"
	And The user adds the developer "dev3" to the activity "the activity"
	Then The list of developers on the first project contains (at least) "dev1" and "dev2"

Scenario: Names on list of developers on project only shows up once
	Given A project exists
	And The user is a project leader
	And The project has an activity with name "the activity"
	And The project has an activity with name "the second activity"
	And A developer "dev1" exists
	And A developer "dev2" exists
	And The user adds the developer "dev1" to the activity "the activity"
	And The user adds the developer "dev1" to the activity "the second activity"
	And The user adds the developer "dev2" to the activity "the second activity"
	Then The names on the list of developers on the first project contains "dev1" and "dev2" and names only show up once each

Scenario: Getting list of developers working on activity
	Given A project exists
	And The user is a project leader
	And The project has an activity with name "the activity"
	And A developer "dev1" exists
	And A developer "dev2" exists
	And The user adds the developer "dev1" to the activity "the activity"
	And The user adds the developer "dev2" to the activity "the activity"
	Then The list of developers on activity "the activity" contains (at least) "dev1" and "dev2"

Scenario: Getting list of projects developer is working on
	Given A project exists
	And Another project exists
	And The user is a project leader
	And The project has an activity with name "the activity"
	And The second project has an activity with name "the next activity"
	And A developer "dev1" exists
	And The user adds the developer "dev1" to the activity "the activity"
	And The user adds the developer "dev1" to the activity "the next activity" on the second project
	Then The list of activities developer "dev1" is working on contains "the activity" from the first project and "the next activity" from the second project

Scenario: Getting list of workers in company
	Given A developer "dev1" exists
	And A developer "dev2" exists
	And A developer "dev3" exists
	Then The list of the devevelopers is "dev1,dev2,dev3"

#Scenario: Getting list of available developers
#	Given Developer "dev1" has registered 8 hours vacation on year 2018 month 02 day 19 from hour 08 minute 00
#	When The user requests a list of available developers from year 2018 month 02 day 19 hour hour 00 minute 00 to year 2018 month 02 day 20 hour 00 minute 00
#	Then The list of available developers does not contain "dev1"
#	And the list of available developers does contain "dev2"
