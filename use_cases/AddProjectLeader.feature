#Author: Magnus
Feature: Add a project leader to a project
	Description: A project leader needs to be added to a project
	Actor: Project leader

Background:
	Given A project exists
	And A developer "JJJJ" exists

Scenario: A project leader assigns himself to the project
	Given The user is a project leader
	When The user assigns "R2D2" as project leader of the project
	And The user looks at the project leader
	Then The user is the project leader of the project

Scenario: A developer assigns himself to the project
	Given The user is a developer
	When The user assigns "R2D2" as project leader of the project
	Then The system informs the user that "You need to be a project leader"

Scenario: A project leader assigns himself to a non-existing project
	Given The user is a project leader
	And The project does not exist
	When The user assigns "R2D2" as project leader of the project
	Then The system informs the user that "That project doesn't not exist"

Scenario: A project leader tries to make a developer project leader
	Given The user is a project leader
	When The user assigns "JJJJ" as project leader of the project
	Then The system informs the user that "A developer cannot be a project leader"

Scenario: A user tries to be project leader while no project leader is specified
	Given The user is a project leader
	When The user looks at the project leader
	Then The system informs the user that "Project leader not specified"