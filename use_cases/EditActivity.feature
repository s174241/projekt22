#Author: Jenath
Feature: Edit Activity
	Description: Edit existing activity in existing project
	Actors: Project leader

Background: Have an existing project
	Given A project exists

Scenario: Edit existing activity
	Given The user is a project leader
	When The user creates activity "test activity" in the project with expected work 12 weeks
	And The user edits activity "test activity" in the project to have name "new activity" and expected work 8 weeks
	Then An activity in the project has name "new activity" with expected work 8 weeks

Scenario: Edit non-existing activity
	Given The user is a project leader
	When The user creates activity "test activity" in the project with expected work 12 weeks
	When The user edits activity "wrong activity" in the project to have name "newest activity" and expected work 12 weeks
	Then The system informs the user that "Unable to find activity to edit"

Scenario: Edit activity without being project leader
	Given The project has an activity with name "test activity"
	And The user is a developer
	When The user edits activity "test activity" in the project to have name "newest activity" and expected work 12 weeks
	Then The system informs the user that "You must be project leader to edit an activity"

Scenario: Edit activity time period
	Given The project has an activity with name "test activity"
	And The user is a project leader
	And The activity "test activity" has a time period from "16-03-2018 08:30:00" to "16-04-2018 20:00:00"
	When The user edits activity "test activity" in the project to have timeperiod "16-04-2018 08:30:00" to "16-07-2018 20:00:00"
	Then The activity "test activity" has time period from "16-04-2018 08:30:00" to "16-07-2018 20:00:00"

Scenario: Edit activity time period without being project leader
	Given The project has an activity with name "test activity"
	And The user is a developer
	And The activity "test activity" has a time period from "16-03-2018 08:30:00" to "16-04-2018 20:00:00"
	When The user edits activity "test activity" in the project to have timeperiod "16-04-2018 08:30:00" to "16-07-2018 20:00:00"
	Then The system informs the user that "You must be project leader to edit an activity"
