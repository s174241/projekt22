#Author: Adam
Feature: Special activity
	Description: Add and edit special activities: sickness, vacation or courses.
	Actors: Employee

Background: Have a developer
	Given The user is a developer

Scenario: Register vacation
	When The developer registers a "vacation" from "16-03-2018 08:30:00" to "22-03-2018 08:30:00"
	Then The developer has registered a vacation from "16-03-2018 08:30:00" to "22-03-2018 08:30:00"
	And The developer is unavailable in the period "16-03-2018 08:30:00" to "22-03-2018 08:30:00"

Scenario: Register sick leave
	When The developer registers a "sick day" from "16-04-2018 09:30:00" to "16-04-2018 17:30:00"
	Then The developer has registered a sick day from "16-04-2018 09:30:00" to "16-04-2018 17:30:00"
	And The developer is unavailable in the period "16-04-2018 09:30:00" to "16-04-2018 17:30:00"

Scenario: Register course
	When The developer registers a "course" from "20-04-2018 07:00:00" to "20-04-2018 11:00:00"
	Then The developer has registered a course from "20-04-2018 07:00:00" to "20-04-2018 11:00:00"
	And The developer is unavailable in the period "20-04-2018 07:00:00" to "20-04-2018 11:00:00"

Scenario: The developer come back from a sick leave and is now available to work on more then 1 activity
	Given A project exists
	And The project has an activity with name "test activity"
	And The activity "test activity" has a time period from "16-03-2018 08:30:00" to "16-04-2018 20:00:00"
	And The user is working on activity "test activity"
	When The developer registers a "sick day" from "16-04-2018 09:30:00" to "16-04-2018 17:30:00"
	Then The developer is available in the period "17-04-2018 08:00:00" to "17-04-2018 17:30:00"

Scenario: Edit vacation
	When The developer registers a "vacation" from "16-03-2018 08:30:00" to "22-03-2018 08:30:00"
	And The developer edits the special registration from "16-03-2018 08:30:00" to "22-03-2018 08:30:00" to be from "16-03-2018 08:30:00" to "24-03-2018 08:30:00"
	Then The developer has registered a vacation from "16-03-2018 08:30:00" to "24-03-2018 08:30:00"
	And The developer is unavailable in the period "16-03-2018 08:30:00" to "24-03-2018 08:30:00"

Scenario: Edit sick leave
	When The developer registers a "sick day" from "16-04-2018 08:30:00" to "16-04-2018 16:30:00"
	And The developer edits the special registration from "16-04-2018 08:30:00" to "16-04-2018 16:30:00" to be from "16-04-2018 08:30:00" to "17-04-2018 08:30:00"
	Then The developer has registered a sick day from "16-04-2018 08:30:00" to "17-04-2018 08:30:00"
	And The developer is unavailable in the period "16-04-2018 08:30:00" to "17-04-2018 08:30:00"

Scenario: Edit course
	When The developer registers a "course" from "20-04-2018 07:00:00" to "20-04-2018 11:00:00"
	And The developer edits the special registration from "20-04-2018 07:00:00" to "20-04-2018 11:00:00" to be from "20-04-2018 07:00:00" to "20-04-2018 12:30:00"
	Then The developer has registered a course from "20-04-2018 07:00:00" to "20-04-2018 12:30:00"
	And The developer is unavailable in the period "20-04-2018 07:00:00" to "20-04-2018 12:30:00"