#Author: Jenath
Feature: AddActivity
	Description: Add new activity to existing project
	Actors: Project leader

Background: Have an existing project
	Given A project exists

Scenario: Create activity to non-existing project
	Given The user is a project leader
	And The project does not exist
	When The user creates activity "test activity" in the project with expected work 12 weeks
	Then The system informs the user that "That project doesn't not exist"

Scenario: User is not project leader
	Given The user is a developer
	When The user creates activity "test activity" in the project with expected work 12 weeks
	Then The system informs the user that "You must be project leader to create an activity"

Scenario: Add activity to project
	Given The user is a project leader
	When The user creates activity "test activity" in the project with expected work 12 weeks
	Then An activity in the project has name "test activity" with expected work 12 weeks

Scenario: Add two activities with the same name
	Given The user is a project leader
	When The user creates activity "test activity" in the project with expected work 12 weeks
	And The user creates activity "test activity" in the project with expected work 10 weeks
	Then The system informs the user that "Activities must have unique names"

Scenario: The projects budget time is the activites added together
	Given The user is a project leader
	When The user creates activity "test activity" in the project with expected work 12 weeks
	And The user creates activity "test activity2" in the project with expected work 10 weeks
	Then The projects budget time is 22 weeks

Scenario: The project leader sets the time period of an activity
	Given The project has an activity with name "activity"
	And The user is a project leader
	When The user sets the time period of the activity "activity" to be from "02-08-2010 09:00:00" to "02-08-2011 09:00:00"
	Then The projects activity "activity" time period is from "02-08-2010 09:00:00" to "02-08-2011 09:00:00"

Scenario: A developer sets the time period of an activity
	Given The project has an activity with name "activity"
	And The user is a developer
	When The user sets the time period of the activity "activity" to be from "02-08-2010 09:00:00" to "02-08-2011 09:00:00"
	Then The system informs the user that "You must be project leader to edit an activity"
