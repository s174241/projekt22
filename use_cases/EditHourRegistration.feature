#Author: Adam
Feature: Edit hour registration as developer
	Description: Developer can edit previous hour registrations
	Actors: Developer

Background: Create project with activity
	Given A project exists
	And The user is a project leader
	And The project has an activity with name "test activity"
	And The user is working on activity "test activity"

Scenario: Edit old time registry
	When The user registers 8 hours on the project on activity "test activity" on "16-03-2018 08:30:00"
	And The developer edits the project and activity "test activity" on "16-03-2018 08:30:00" to instead 6 hours
	Then The user has 6 hours registered in the project on activity "test activity" on "16-03-2018 08:30:00"

Scenario: Edit old time registry, which does not exist
	When The developer edits the project and activity "test activity" on "16-03-1970 08:30:00" to instead 6 hours
	Then The system informs the user that "The selected time registry does not exist"