#Author: Adam
Feature: Register hours as developer
	Description: Developer enters the hours he/she has worked on an activity
	Actors: Developer

Scenario: Project does not exist
	Given The user is a developer
	When The user registers 8 hours on the project on activity "test activity" on "16-03-2018 08:30:00"
	Then The system informs the user that "That project doesn't not exist"

Scenario: Activity does not exist
	Given A project exists
	And The user is a developer
	When The user registers 8 hours on the project on activity "wrong activity" on "16-03-2018 08:30:00"
	Then The system informs the user that "Unable to find activity to edit"

Scenario: Register hours worked on an activity in a project
	Given A project exists
	And The user is a developer
	And The project has an activity with name "test activity"
	And The user is working on activity "test activity"
	When The user registers 8 hours on the project on activity "test activity" on "16-03-2018 08:30:00"
	Then The user has 8 hours registered in the project on activity "test activity" on "16-03-2018 08:30:00"
	And The project has 8 hours registered in period "16-03-2018 08:00:00" to "16-03-2018 20:00:00"

Scenario: Register hours on activity developer is not working on
	Given A project exists
	And The user is a developer
	And The project has an activity with name "not working on"
	When The user registers 8 hours on the project on activity "not working on" on "16-03-2018 08:30:00"
	Then The user has 8 hours registered in the project on activity "not working on" on "16-03-2018 08:30:00"

Scenario: Register hours outside of activitys timePeriod
	Given A project exists
	And The user is a developer
	And The project has an activity with name "period activity"
	And The activity "period activity" has a time period from "01-01-2018 08:00:00" to "01-02-2018 08:00:00"
	When The user registers 6 hours on the project on activity "period activity" on "01-03-2018 08:00:00"
	Then The system informs the user that "Date is outside activity's timePeriod"
	