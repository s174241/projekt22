#Author: Philip
Feature: Remove developer from activity
	Description: Developers can be removed from an activity
	Actors: Project leader, Developer

Background:
	Given A project exists
	And The project has an activity with name "test activity"
	And A developer "JJJJ" exists

Scenario: Project leader removes a developer from an activity
	Given The user is a project leader
	And The user adds the developer "JJJJ" to the activity "test activity"
	When The user removes the developer "JJJJ" from the activity "test activity"
	Then The developer "JJJJ" is no longer assigned to the activity "test activity"

Scenario: Project leader removes a developer that does not work on the activity
	Given The user is a project leader
	When The user removes the developer "JJJJ" from the activity "test activity"
	Then The system informs the user that "The user does not work on this activity"

Scenario: The project leader removes a non-existing developer
	Given The user is a project leader
	When The user removes the developer "OOOO" from the activity "test activity"
	Then The system informs the user that "User doesn't exist"