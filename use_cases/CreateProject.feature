#Author: Magnus
Feature: Create project
	Description: A Project Leader creates a project
	Actor: Project Leader

Scenario: Project Leader creates a project with a name
	Given The user is a project leader
	When The user creates a project with name "Test_Name"
	Then A project exists with name "Test_Name"

Scenario: Project Leader tries to create a project without a name
	Given The user is a project leader
	When The user creates a project with name ""
	Then A project exists with name ""

Scenario: Project Leader changes name of project
	Given A project exists
	And The user is a project leader
	When The user changes the name of the project to "Test_Name"
	Then A project exists with name "Test_Name"

 Scenario: Non project leader creates a project
	Given The user is a developer
 	When The user creates a project with name "Test_Name"
 	Then The system informs the user that "You need to be a project leader to create a project"

Scenario: Non project leader tries to change name of a project
	Given A project exists
	And The user is a developer
	When The user changes the name of the project to "Test_Name"
	Then The system informs the user that "You need to be project leader to change the name of a project"

Scenario: A project leader tries to change the name of a project that does not exist
	Given The user is a project leader
	And The project does not exist
	When The user changes the name of the project to "Test_Name"
	Then The system informs the user that "That project doesn't not exist"

Scenario: Setting a projects timeperiod
	Given A project exists
	And The user is a project leader
	When The user sets the time period of the project to be from "02-08-2010 08:30:00" to "02-08-2011 08:30:00"
	Then The projects time period is from "02-08-2010 08:30:00" to "02-08-2011 08:30:00"

#Scenario: Getting valid project id
#	Given A project exists
#	Then the project has a valid id
#
#Scenario: Creating too many projects
#	Given 9999 projects exist
#	When Another project is created
#	Then the last projects id is -1
#	And The system informs the user that "No ID available for project"




