#Author: Philip
Feature: Add developer to activity
	Description: Developers can be added to an activity and the company
	Actors: Project leader, Developer

Background:
	Given A project exists
	And The project has an activity with name "test activity"
	And A developer "JJJJ" exists
	And A developer "KKKK" exists

Scenario: Two user with the same initials
	When A developer "JJJJ" exists
	Then The system informs the user that "An employee with those initials is already registered in the company"

Scenario: Project leader adds a developer to an activity
	Given The user is a project leader
	When The user adds the developer "JJJJ" to the activity "test activity"
	Then The developer "JJJJ" is assigned to the activity "test activity"

Scenario: Project leader tries to add a non-existing developer to an activity
	Given The user is a project leader
	When The user adds the developer "OOOO" to the activity "test activity"
	Then The system informs the user that "User doesn't exist"

Scenario: Project leader tries to add developer to a non-existing activity
	Given The user is a project leader
	And The project does not exist
	When The user adds the developer "JJJJ" to the activity "test activity"
	Then The system informs the user that "That project doesn't not exist"

Scenario: Adding two developers to same activity
	Given The user is a project leader
	When The user adds the developer "JJJJ" to the activity "test activity"
	And The user adds the developer "KKKK" to the activity "test activity"
	Then The developer "JJJJ" is assigned to the activity "test activity"
	Then The developer "KKKK" is assigned to the activity "test activity"

Scenario: Date of creation is stored for each developer
	When A developer "NY" is added on "25-04-2018 08:00:00"
	Then Developer "NY" has creation date "25-04-2018 08:00:00"