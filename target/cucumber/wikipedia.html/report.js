$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("use_cases/AddActivity.feature");
formatter.feature({
  "name": "AddActivity",
  "description": "\tDescription: Add new activity to existing project\n\tActors: Project leader",
  "keyword": "Feature"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Create activity to non-existing project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project does not exist",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.the_project_does_not_exist()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity\" in the project with expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"That project doesn\u0027t not exist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User is not project leader",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity\" in the project with expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"You must be project leader to create an activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Add activity to project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity\" in the project with expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "An activity in the project has name \"test activity\" with expected work 12 weeks",
  "keyword": "Then "
});
formatter.match({
  "location": "ActivitySteps.anActivityInTheProjectHasNameWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Add two activities with the same name",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity\" in the project with expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity\" in the project with expected work 10 weeks",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"Activities must have unique names\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "The projects budget time is the activites added together",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity\" in the project with expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity2\" in the project with expected work 10 weeks",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The projects budget time is 22 weeks",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.theProjectsBudgetTimeIsWeeks(int)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "The project leader sets the time period of an activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The project has an activity with name \"activity\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user sets the time period of the activity \"activity\" to be from \"02-08-2010 09:00:00\" to \"02-08-2011 09:00:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.userSetsTimePriodOfActivity(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The projects activity \"activity\" time period is from \"02-08-2010 09:00:00\" to \"02-08-2011 09:00:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ActivitySteps.ensureActivityTimePeriod(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A developer sets the time period of an activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The project has an activity with name \"activity\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user sets the time period of the activity \"activity\" to be from \"02-08-2010 09:00:00\" to \"02-08-2011 09:00:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.userSetsTimePriodOfActivity(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"You must be project leader to edit an activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/AddDeveloper.feature");
formatter.feature({
  "name": "Add developer to activity",
  "description": "\tDescription: Developers can be added to an activity and the company\n\tActors: Project leader, Developer",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"KKKK\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Two user with the same initials",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"An employee with those initials is already registered in the company\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"KKKK\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Project leader adds a developer to an activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"JJJJ\" to the activity \"test activity\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer \"JJJJ\" is assigned to the activity \"test activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "UserSteps.theDeveloperIsAssignedToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"KKKK\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Project leader tries to add a non-existing developer to an activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"OOOO\" to the activity \"test activity\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"User doesn\u0027t exist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"KKKK\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Project leader tries to add developer to a non-existing activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project does not exist",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.the_project_does_not_exist()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"JJJJ\" to the activity \"test activity\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"That project doesn\u0027t not exist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"KKKK\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Adding two developers to same activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"JJJJ\" to the activity \"test activity\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"KKKK\" to the activity \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer \"JJJJ\" is assigned to the activity \"test activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "UserSteps.theDeveloperIsAssignedToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer \"KKKK\" is assigned to the activity \"test activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "UserSteps.theDeveloperIsAssignedToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"KKKK\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Date of creation is stored for each developer",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A developer \"NY\" is added on \"25-04-2018 08:00:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.aDeveloperIsAddedOn(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Developer \"NY\" has creation date \"25-04-2018 08:00:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "UserSteps.developerHasCreationDate(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/AddProjectLeader.feature");
formatter.feature({
  "name": "Add a project leader to a project",
  "description": "\tDescription: A project leader needs to be added to a project\n\tActor: Project leader",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A project leader assigns himself to the project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user assigns \"R2D2\" as project leader of the project",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserAssignsAsProjectLeaderOfTheProject(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user looks at the project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserLooksAtTheProjectLeader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is the project leader of the project",
  "keyword": "Then "
});
formatter.match({
  "location": "UserSteps.theUserIsTheProjectLeaderOfTheProject()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A developer assigns himself to the project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user assigns \"R2D2\" as project leader of the project",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserAssignsAsProjectLeaderOfTheProject(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"You need to be a project leader\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A project leader assigns himself to a non-existing project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project does not exist",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.the_project_does_not_exist()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user assigns \"R2D2\" as project leader of the project",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserAssignsAsProjectLeaderOfTheProject(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"That project doesn\u0027t not exist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A project leader tries to make a developer project leader",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user assigns \"JJJJ\" as project leader of the project",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserAssignsAsProjectLeaderOfTheProject(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"A developer cannot be a project leader\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A user tries to be project leader while no project leader is specified",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user looks at the project leader",
  "keyword": "When "
});
formatter.match({
  "location": "UserSteps.theUserLooksAtTheProjectLeader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"Project leader not specified\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/CreateProject.feature");
formatter.feature({
  "name": "Create project",
  "description": "\tDescription: A Project Leader creates a project\n\tActor: Project Leader",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Project Leader creates a project with a name",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates a project with name \"Test_Name\"",
  "keyword": "When "
});
formatter.match({
  "location": "ProjectSteps.the_user_creates_a_project_with_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A project exists with name \"Test_Name\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists_with_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Project Leader tries to create a project without a name",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates a project with name \"\"",
  "keyword": "When "
});
formatter.match({
  "location": "ProjectSteps.the_user_creates_a_project_with_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A project exists with name \"\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists_with_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Project Leader changes name of project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user changes the name of the project to \"Test_Name\"",
  "keyword": "When "
});
formatter.match({
  "location": "ProjectSteps.the_user_changes_the_name_of_the_project_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A project exists with name \"Test_Name\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists_with_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Non project leader creates a project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates a project with name \"Test_Name\"",
  "keyword": "When "
});
formatter.match({
  "location": "ProjectSteps.the_user_creates_a_project_with_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"You need to be a project leader to create a project\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Non project leader tries to change name of a project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user changes the name of the project to \"Test_Name\"",
  "keyword": "When "
});
formatter.match({
  "location": "ProjectSteps.the_user_changes_the_name_of_the_project_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"You need to be project leader to change the name of a project\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "A project leader tries to change the name of a project that does not exist",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project does not exist",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.the_project_does_not_exist()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user changes the name of the project to \"Test_Name\"",
  "keyword": "When "
});
formatter.match({
  "location": "ProjectSteps.the_user_changes_the_name_of_the_project_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"That project doesn\u0027t not exist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Setting a projects timeperiod",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user sets the time period of the project to be from \"02-08-2010 08:30:00\" to \"02-08-2011 08:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "ProjectSteps.userSetsTimePeriod(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The projects time period is from \"02-08-2010 08:30:00\" to \"02-08-2011 08:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.ensureTimePeriod(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/EditActivity.feature");
formatter.feature({
  "name": "Edit Activity",
  "description": "\tDescription: Edit existing activity in existing project\n\tActors: Project leader",
  "keyword": "Feature"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit existing activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity\" in the project with expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user edits activity \"test activity\" in the project to have name \"new activity\" and expected work 8 weeks",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theUserEditsActivityInTheProjectToHaveNameAndExpectedWorkWeeks(String,String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "An activity in the project has name \"new activity\" with expected work 8 weeks",
  "keyword": "Then "
});
formatter.match({
  "location": "ActivitySteps.anActivityInTheProjectHasNameWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit non-existing activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user creates activity \"test activity\" in the project with expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user edits activity \"wrong activity\" in the project to have name \"newest activity\" and expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserEditsActivityInTheProjectToHaveNameAndExpectedWorkWeeks(String,String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"Unable to find activity to edit\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit activity without being project leader",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user edits activity \"test activity\" in the project to have name \"newest activity\" and expected work 12 weeks",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserEditsActivityInTheProjectToHaveNameAndExpectedWorkWeeks(String,String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"You must be project leader to edit an activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit activity time period",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The activity \"test activity\" has a time period from \"16-03-2018 08:30:00\" to \"16-04-2018 20:00:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theActivityHasATimePeriodFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user edits activity \"test activity\" in the project to have timeperiod \"16-04-2018 08:30:00\" to \"16-07-2018 20:00:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserEditsActivityInTheProjectToHaveTimeperiodTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The activity \"test activity\" has time period from \"16-04-2018 08:30:00\" to \"16-07-2018 20:00:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ActivitySteps.theActivityHasTimePeriodFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have an existing project",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit activity time period without being project leader",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "Given "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The activity \"test activity\" has a time period from \"16-03-2018 08:30:00\" to \"16-04-2018 20:00:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theActivityHasATimePeriodFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user edits activity \"test activity\" in the project to have timeperiod \"16-04-2018 08:30:00\" to \"16-07-2018 20:00:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserEditsActivityInTheProjectToHaveTimeperiodTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"You must be project leader to edit an activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/EditHourRegistration.feature");
formatter.feature({
  "name": "Edit hour registration as developer",
  "description": "\tDescription: Developer can edit previous hour registrations\n\tActors: Developer",
  "keyword": "Feature"
});
formatter.background({
  "name": "Create project with activity",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is working on activity \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserIsWorkingOnActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit old time registry",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user registers 8 hours on the project on activity \"test activity\" on \"16-03-2018 08:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserRegistersHoursOnTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer edits the project and activity \"test activity\" on \"16-03-2018 08:30:00\" to instead 6 hours",
  "keyword": "And "
});
formatter.match({
  "location": "EditHourRegistrationSteps.theDeveloperEditsTheProjectAndActivityOnYearMonthDayToInsteadRegisterHours(String,String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user has 6 hours registered in the project on activity \"test activity\" on \"16-03-2018 08:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserHasHoursRegisteredInTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Create project with activity",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is working on activity \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserIsWorkingOnActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit old time registry, which does not exist",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The developer edits the project and activity \"test activity\" on \"16-03-1970 08:30:00\" to instead 6 hours",
  "keyword": "When "
});
formatter.match({
  "location": "EditHourRegistrationSteps.theDeveloperEditsTheProjectAndActivityOnYearMonthDayToInsteadRegisterHours(String,String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"The selected time registry does not exist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/GettingWorkersInProjects.feature");
formatter.feature({
  "name": "Getting users from projects or likewise",
  "description": "\tDescription: Different tests regarding getting specific users from a list\n\tActors: Developer",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Getting list of developers working on project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev1\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev2\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev1\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev2\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The list of developers on the first project contains (at least) \"dev1\" and \"dev2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.theListOfDevelopersOnTheFirstProjectContainsAtLeastAnd(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Getting list of developers working on project, when devs are working on different activities",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"the second activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev1\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev2\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev1\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev2\" to the activity \"the second activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The list of developers on the first project contains (at least) \"dev1\" and \"dev2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.theListOfDevelopersOnTheFirstProjectContainsAtLeastAnd(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Other developers are allowed on the list of workers on project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev1\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev2\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev3\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev1\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev2\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev3\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The list of developers on the first project contains (at least) \"dev1\" and \"dev2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.theListOfDevelopersOnTheFirstProjectContainsAtLeastAnd(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Names on list of developers on project only shows up once",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"the second activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev1\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev2\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev1\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev1\" to the activity \"the second activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev2\" to the activity \"the second activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The names on the list of developers on the first project contains \"dev1\" and \"dev2\" and names only show up once each",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.theNamesOnTheListOfDevelopersOnTheFirstProjectContainsAndAndNamesOnlyShowUpOnceEach(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Getting list of developers working on activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev1\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev2\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev1\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev2\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The list of developers on activity \"the activity\" contains (at least) \"dev1\" and \"dev2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.theListOfDevelopersOnActivityContainsAtLeastAnd(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Getting list of projects developer is working on",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Another project exists",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.anotherProjectExists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The second project has an activity with name \"the next activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theSecondProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev1\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev1\" to the activity \"the activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"dev1\" to the activity \"the next activity\" on the second project",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivityOnTheSecondProject(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The list of activities developer \"dev1\" is working on contains \"the activity\" from the first project and \"the next activity\" from the second project",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.theListOfActivitiesDeveloperIsWorkingOnContainsFromTheFirstProjectAndFromTheSecondProject(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Getting list of workers in company",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A developer \"dev1\" exists",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev2\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"dev3\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The list of the devevelopers is \"dev1,dev2,dev3\"",
  "keyword": "Then "
});
formatter.match({
  "location": "UserSteps.theListOfTheDevevelopersIs(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/RegisterSpecialActivity.feature");
formatter.feature({
  "name": "Special activity",
  "description": "\tDescription: Add and edit special activities: sickness, vacation or courses.\n\tActors: Employee",
  "keyword": "Feature"
});
formatter.background({
  "name": "Have a developer",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Register vacation",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The developer registers a \"vacation\" from \"16-03-2018 08:30:00\" to \"22-03-2018 08:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "SpecialActivitySteps.registersVacation(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer has registered a vacation from \"16-03-2018 08:30:00\" to \"22-03-2018 08:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureRegistration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer is unavailable in the period \"16-03-2018 08:30:00\" to \"22-03-2018 08:30:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureUnavailability(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have a developer",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Register sick leave",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The developer registers a \"sick day\" from \"16-04-2018 09:30:00\" to \"16-04-2018 17:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "SpecialActivitySteps.registersVacation(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer has registered a sick day from \"16-04-2018 09:30:00\" to \"16-04-2018 17:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureRegistration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer is unavailable in the period \"16-04-2018 09:30:00\" to \"16-04-2018 17:30:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureUnavailability(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have a developer",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Register course",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The developer registers a \"course\" from \"20-04-2018 07:00:00\" to \"20-04-2018 11:00:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "SpecialActivitySteps.registersVacation(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer has registered a course from \"20-04-2018 07:00:00\" to \"20-04-2018 11:00:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureRegistration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer is unavailable in the period \"20-04-2018 07:00:00\" to \"20-04-2018 11:00:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureUnavailability(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have a developer",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "The developer come back from a sick leave and is now available to work on more then 1 activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The activity \"test activity\" has a time period from \"16-03-2018 08:30:00\" to \"16-04-2018 20:00:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theActivityHasATimePeriodFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is working on activity \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserIsWorkingOnActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer registers a \"sick day\" from \"16-04-2018 09:30:00\" to \"16-04-2018 17:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "SpecialActivitySteps.registersVacation(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer is available in the period \"17-04-2018 08:00:00\" to \"17-04-2018 17:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SpecialActivitySteps.theDeveloperIsAvailableInThePeriodTo(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have a developer",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit vacation",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The developer registers a \"vacation\" from \"16-03-2018 08:30:00\" to \"22-03-2018 08:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "SpecialActivitySteps.registersVacation(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer edits the special registration from \"16-03-2018 08:30:00\" to \"22-03-2018 08:30:00\" to be from \"16-03-2018 08:30:00\" to \"24-03-2018 08:30:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.theDeveloperEditsTheSpecialRegistrationFromToToBeFromTo(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer has registered a vacation from \"16-03-2018 08:30:00\" to \"24-03-2018 08:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureRegistration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer is unavailable in the period \"16-03-2018 08:30:00\" to \"24-03-2018 08:30:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureUnavailability(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have a developer",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit sick leave",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The developer registers a \"sick day\" from \"16-04-2018 08:30:00\" to \"16-04-2018 16:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "SpecialActivitySteps.registersVacation(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer edits the special registration from \"16-04-2018 08:30:00\" to \"16-04-2018 16:30:00\" to be from \"16-04-2018 08:30:00\" to \"17-04-2018 08:30:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.theDeveloperEditsTheSpecialRegistrationFromToToBeFromTo(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer has registered a sick day from \"16-04-2018 08:30:00\" to \"17-04-2018 08:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureRegistration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer is unavailable in the period \"16-04-2018 08:30:00\" to \"17-04-2018 08:30:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureUnavailability(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "Have a developer",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Edit course",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The developer registers a \"course\" from \"20-04-2018 07:00:00\" to \"20-04-2018 11:00:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "SpecialActivitySteps.registersVacation(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer edits the special registration from \"20-04-2018 07:00:00\" to \"20-04-2018 11:00:00\" to be from \"20-04-2018 07:00:00\" to \"20-04-2018 12:30:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.theDeveloperEditsTheSpecialRegistrationFromToToBeFromTo(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer has registered a course from \"20-04-2018 07:00:00\" to \"20-04-2018 12:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureRegistration(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer is unavailable in the period \"20-04-2018 07:00:00\" to \"20-04-2018 12:30:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecialActivitySteps.ensureUnavailability(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/RemoveDeveloper.feature");
formatter.feature({
  "name": "Remove developer from activity",
  "description": "\tDescription: Developers can be removed from an activity\n\tActors: Project leader, Developer",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Project leader removes a developer from an activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user adds the developer \"JJJJ\" to the activity \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserAddsTheDeveloperToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user removes the developer \"JJJJ\" from the activity \"test activity\"",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserRemovesTheDeveloperFromTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The developer \"JJJJ\" is no longer assigned to the activity \"test activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ActivitySteps.theDeveloperIsNoLongerAssignedToTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Project leader removes a developer that does not work on the activity",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user removes the developer \"JJJJ\" from the activity \"test activity\"",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserRemovesTheDeveloperFromTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"The user does not work on this activity\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A developer \"JJJJ\" exists",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.aDeveloperExists(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "The project leader removes a non-existing developer",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a project leader",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_project_leader()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user removes the developer \"OOOO\" from the activity \"test activity\"",
  "keyword": "When "
});
formatter.match({
  "location": "ActivitySteps.theUserRemovesTheDeveloperFromTheActivity(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"User doesn\u0027t exist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("use_cases/TimeRegistration.feature");
formatter.feature({
  "name": "Register hours as developer",
  "description": "\tDescription: Developer enters the hours he/she has worked on an activity\n\tActors: Developer",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Project does not exist",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "Given "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user registers 8 hours on the project on activity \"test activity\" on \"16-03-2018 08:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserRegistersHoursOnTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"That project doesn\u0027t not exist\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Activity does not exist",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user registers 8 hours on the project on activity \"wrong activity\" on \"16-03-2018 08:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserRegistersHoursOnTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"Unable to find activity to edit\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Register hours worked on an activity in a project",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is working on activity \"test activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.theUserIsWorkingOnActivity(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user registers 8 hours on the project on activity \"test activity\" on \"16-03-2018 08:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserRegistersHoursOnTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user has 8 hours registered in the project on activity \"test activity\" on \"16-03-2018 08:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserHasHoursRegisteredInTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has 8 hours registered in period \"16-03-2018 08:00:00\" to \"16-03-2018 20:00:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "TimeRegistrationSteps.theProjectHasHoursRegisteredInPeriodTo(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Register hours on activity developer is not working on",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"not working on\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user registers 8 hours on the project on activity \"not working on\" on \"16-03-2018 08:30:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserRegistersHoursOnTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user has 8 hours registered in the project on activity \"not working on\" on \"16-03-2018 08:30:00\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserHasHoursRegisteredInTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Register hours outside of activitys timePeriod",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "A project exists",
  "keyword": "Given "
});
formatter.match({
  "location": "ProjectSteps.a_project_exists()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user is a developer",
  "keyword": "And "
});
formatter.match({
  "location": "UserSteps.the_user_is_a_developer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The project has an activity with name \"period activity\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theProjectHasAnActivityWithName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The activity \"period activity\" has a time period from \"01-01-2018 08:00:00\" to \"01-02-2018 08:00:00\"",
  "keyword": "And "
});
formatter.match({
  "location": "ActivitySteps.theActivityHasATimePeriodFromTo(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user registers 6 hours on the project on activity \"period activity\" on \"01-03-2018 08:00:00\"",
  "keyword": "When "
});
formatter.match({
  "location": "TimeRegistrationSteps.theUserRegistersHoursOnTheProjectOnActivityOn(int,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The system informs the user that \"Date is outside activity\u0027s timePeriod\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StandardSteps.theSystemInformsTheUserThat(String)"
});
formatter.result({
  "status": "passed"
});
});