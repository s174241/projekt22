package project22.exceptions;

/**
 *
 * @author Magnus
 *
 */
public class AlreadyExistsException extends Exception{

	private static final long serialVersionUID = -4503310826752657652L;

	public AlreadyExistsException(String message) {
		super(message);
	}

}
