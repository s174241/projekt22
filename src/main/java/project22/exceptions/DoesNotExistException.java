package project22.exceptions;
/**
 *
 * @author Magnus
 *
 */
public class DoesNotExistException extends Exception {

	private static final long serialVersionUID = -492327095114715636L;

	public DoesNotExistException(String message) {
		super(message);
	}
}
