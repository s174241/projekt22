package project22.exceptions;

/**
 *
 * @author Philip
 *
 */
public class UnauthorizedException extends Exception {
	private static final long serialVersionUID = -3096658971904048677L;

	public UnauthorizedException(String message) {
		super(message);
	}
}
