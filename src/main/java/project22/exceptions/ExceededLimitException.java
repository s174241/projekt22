package project22.exceptions;

/**
 *
 * @author Magnus
 *
 */
public class ExceededLimitException extends Exception {

	private static final long serialVersionUID = 7283788778825887485L;

	public ExceededLimitException(String message) {
		super(message);
	}

}
