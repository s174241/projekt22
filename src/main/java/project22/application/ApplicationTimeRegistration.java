package project22.application;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import project22.domain.Activity;
import project22.domain.SpecialActivity;
import project22.domain.SpecialActivity.Type;
import project22.domain.TimePeriod;
import project22.domain.TimeRegistration;
import project22.domain.User;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;

/**
 *
 * @author Jenath
 *
 */
public class ApplicationTimeRegistration {

	ApplicationInfo appInfo;

	public ApplicationTimeRegistration(ApplicationInfo appInfo) {
		this.appInfo = appInfo;
	}

	/**
	 * Register hours on activity with name activityName for current user on date
	 * date.
	 *
	 * @throws DoesNotExistException
	 *             if the activity does not exist
	 * @param hours
	 *            the hours to be registered
	 * @param activityName
	 *            the name of the activity to register hours to
	 * @param date
	 *            the date the hours should be registered on
	 * @throws ExceededLimitException 
	 *            the date is outside the activity's timePeriod
	 * 
	 */
	public void registerHoursOnActivity(String activityName, LocalDateTime date, int hours)
			throws DoesNotExistException, AlreadyExistsException, ExceededLimitException {
		TimePeriod period = appInfo.getActivity(activityName).getTimePeriod();
		if (period != null && !period.contains(date))
				throw new ExceededLimitException("Date is outside activity's timePeriod");
		appInfo.getActivity(activityName).addTimeRegistration(new TimeRegistration(date, hours, appInfo.currentUser));
	}

	/**
	 * Edit hours registered on activity with name activityName for current user on date
	 * date.
	 *
	 * @throws DoesNotExistException
	 *             if the activity does not exist
	 * @param newHours
	 *            the new hours to be registered
	 * @param activityName
	 *            the name of the activity to register hours to
	 * @param date
	 *            the date the hours should be registered on
	 * @throws ExceededLimitException 
	 *            the date is outside Activity's timePeriod
	 */
	public void editHourRegisteredOnActivity(String activityName, LocalDateTime date, int newHours) throws DoesNotExistException, AlreadyExistsException, ExceededLimitException {
		if(!appInfo.getActivity(activityName).removeTimeRegistration(appInfo.currentUser, date)){
			throw new DoesNotExistException("The selected time registry does not exist");
		}
		registerHoursOnActivity(activityName, date, newHours);
	}


	/**
	 * Gets hours the current user has worked on the activity on the date.
	 *
	 * @return the hours worked
	 * @throws DoesNotExistException
	 *             if the activity does not exist
	 * @param activityName
	 *            the name of the activity
	 * @param date
	 *            the date the hours are registered to
	 */
	public int getUserHoursOnActivity(String activityName, LocalDateTime date) throws DoesNotExistException {
		return appInfo.getActivity(activityName).getHoursWorkedOn(appInfo.currentUser, date);
	}

	/**
	 * Registers a special activity in the given period for the current user.
	 *
	 * @param type
	 *            the type of the special activity
	 * @param period
	 *            the TimePeriod the activity takes place in
	 * @throws DoesNotExistException
	 */
	public void registerSpecialActivity(String typeText, TimePeriod period){
		//pre: typeText and period is not null
		//pre: typeText is an element in the set of possible special activity types
		//pre: current user exists
		assert appInfo.currentUser != null;
		assert typeText != null && period != null;
		assert Stream.of("vacation","sick day","course").anyMatch(s -> s.equals(typeText));

		Type type = null;
		if (typeText.equals("vacation"))                  // 1
			type = SpecialActivity.Type.VACATION;
		else if (typeText.equals("sick day"))             // 2
			type = SpecialActivity.Type.SICK_LEAVE;
		else                                              // 3
			type = SpecialActivity.Type.WORK_RELATED;

		SpecialActivity activity = new SpecialActivity(type, period);
		appInfo.currentUser.addActivity(activity);

		//post: currentUser.activities contains a special activity in the period of the type
		assert appInfo.currentUser.getActivities().contains(activity);
		assert activity.getType() == type;
		assert activity.getTimePeriod().equals(period);
	}

	/**
	 * Gets the special activity which happens in the period.
	 *
	 * @return the special activity from the TimePeriod period
	 * @param period
	 *            the TimePeriod the special activity takes place
	 */
	public Activity getSpecialActivityInPeriod(TimePeriod period) {
		return appInfo.currentUser.getSpecialActivityInPeriod(period);
	}

	/**
	 * Check if the user with those initials is available in the given period.
	 *
	 * @return if the user with initials initials is available in the TimePeriod
	 *         period
	 * @throws DoesNotExistException
	 *             if the user does not exist
	 */
	public boolean isUserAvailable(User user, TimePeriod period) throws DoesNotExistException {
		return user.isAvailable(period);
	}

	/**
	 * Changes the TimePeriod of a special activity with TimePeriod from.
	 *
	 * @param from
	 *            the previous TimePeriod
	 * @param to
	 *            the new TimePeriod
	 */
	public void editSpecialActivity(TimePeriod from, TimePeriod to) {
		SpecialActivity activity = (SpecialActivity) getSpecialActivityInPeriod(from);
		activity.setTimePeriod(to);
		activity.setName(activity.getType().toString() + to.toString());
	}


}
