package project22.application;

import java.util.List;
import java.util.stream.Collectors;

import project22.domain.Activity;
import project22.domain.ProjectActivity;
import project22.domain.TimePeriod;
import project22.domain.User;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.UnauthorizedException;

/**
 *
 * @author Magnus
 *
 */
public class ApplicationActivity {

	private ApplicationInfo appInfo;

	public ApplicationActivity(ApplicationInfo appInfo) {
		this.appInfo = appInfo;
	}

	/**
	 * Creates an activity with the given name if the current user is a project
	 * leader.
	 *
	 * @throws UnauthorizedException
	 *             if the current user is not a project leader
	 * @throws DoesNotExistException
	 *             if no project has been opened
	 * @throws AlreadyExistsException
	 *             if an activity with that name already exist
	 * @param activityName
	 *            the name of the activity
	 */
	public void createActivity(String activityName)
			throws UnauthorizedException, DoesNotExistException, AlreadyExistsException {
		appInfo.ensureProjectLeader("You must be project leader to create an activity");
		adminCreateActivity(activityName);
	}

	/**
	 * Forcefully creates an activity with the activityName.
	 *
	 * @throws DoesNotExistException
	 *             if no project has been opened
	 * @throws AlreadyExistsException
	 *             if an activity with that name already exists
	 * @param activityName
	 *            The name of the activity
	 */
	public void adminCreateActivity(String activityName) throws DoesNotExistException, AlreadyExistsException {
		appInfo.ensureOpenProject();
		appInfo.openProject.addActivity(new ProjectActivity(activityName));
	}

	/**
	 * Retrieves an activity by name.
	 *
	 * @return the activity with the given name
	 * @throws UnauthorizedException
	 *             if the current user is not a project leader
	 * @throws DoesNotExistException
	 *             if an activity with that name does not exist
	 */
	private Activity getActivityToEdit(String activityName) throws UnauthorizedException, DoesNotExistException {
		appInfo.ensureProjectLeader("You must be project leader to edit an activity");
		return appInfo.getActivity(activityName);
	}

	/**
	 * Retrieves an activity by name, and sets its budgeted time to the given number
	 * of hours.
	 *
	 * @throws UnauthorizedException
	 *             if the user is not a project leader
	 * @throws DoesNotExistException
	 *             if an activity with that name does not exist
	 */
	public void setActivityBudgetTime(String activityName, int hours)
			throws UnauthorizedException, DoesNotExistException {
		getActivityToEdit(activityName).setBudgetedTime(hours);
	}

	/**
	 * Gets the budget time of the activity with name activityName
	 *
	 * @throws DoesNotExistException
	 *             if no activity has that name
	 * @param activityName
	 *            the name of the activity
	 * @return the budget time of the activity with name activityName
	 */
	public int getBudgetTimeOfActivity(String activityName) throws DoesNotExistException {
		return appInfo.getActivity(activityName).getBudgetedTime();
	}

	/**
	 * Sets the name of the activity with name activityName to newName
	 *
	 * @throws UnauthorizedException
	 *             if the user is not a project leader
	 * @throws DoesNotExistException
	 *             if no activity has that name
	 * @param activityName
	 *            The name of the activity before changing
	 * @param newName
	 *            The name of the activity after changing
	 */
	public void setActivityName(String activityName, String newName)
			throws UnauthorizedException, DoesNotExistException {
		getActivityToEdit(activityName).setName(newName);
	}

	/**
	 * Removes the user from the activity
	 *
	 * @throws DoesNotExistException
	 *             if the user does not exist or if the activity does not exist
	 * @throws UnauthorizedException
	 *             if the current user is not a project leader
	 * @param activity2
	 *            The name of the activity
	 * @param initials
	 *            The initials of the user specified
	 */
	public void removeUserFromActivity(Activity activity, String initials)
			throws DoesNotExistException, UnauthorizedException {
		User user = appInfo.getUser(initials);
		if (!activity.getUsers().contains(user))
			throw new DoesNotExistException("The user does not work on this activity");
		activity.removeUser(user);
	}

	/**
	 * Binds the user to the activity with name activityName if the current user is
	 * a project leader
	 *
	 * @throws UnauthorizedException
	 *             if current the user is not a project leader
	 * @throws DoesNotExistException
	 *             if the current user or the activity does not exist
	 * @param activity
	 *            The name of the activity
	 * @param initials
	 *            The initials of the user
	 */
	public void addUserToActivity(Activity activity, String initials)
			throws UnauthorizedException, DoesNotExistException {
		appInfo.ensureProjectLeader("You must be project leader to edit an activity");
		adminAddUserToActivity(activity, initials);
	}

	/**
	 * Force-binds the user to the activity with name activityName
	 *
	 * @throws DoesNotExistException
	 *             if the user or activity does not exist
	 * @param activity
	 *            The name of the activity
	 * @param initials
	 *            The initials of the user
	 */
	public void adminAddUserToActivity(Activity activity, String initials) throws DoesNotExistException {
		activity.addUser(appInfo.getUser(initials));
	}

	/**
	 * Gets a list of initials of the workers of the activity with name activityName
	 *
	 * @throws DoesNotExistException
	 *             if the activity does not exist
	 * @param activity
	 *            The name of the activity
	 * @return A list of the initials of the workers of the activity
	 */
	public List<String> getWorkersOfActivity(Activity activity) throws DoesNotExistException {
		return activity.getUsers().stream()
				.map(user -> user.getInitials())
				.collect(Collectors.toList());
	}

	/**
	 * Sets the time period of the project, if the current user is a project leader
	 *
	 * @throws UnauthorizedException
	 *             if the current user is not a project leader
	 * @throws DoesNotExistException
	 *             if the activity does not exist
	 * @param activityName
	 *            The name of the activity
	 * @param period
	 *            The TimePeriod the activity should have
	 */
	public void setActivityTimePeriod(String activityName, TimePeriod period)
			throws UnauthorizedException, DoesNotExistException {
		appInfo.ensureProjectLeader("You must be project leader to edit an activity");
		adminSetActivityTimePeriod(activityName, period);
	}

	/**
	 * An override to set an activities TimePeriod. WARNING: SHOULD ONLY BE USED FOR
	 * TESTING
	 *
	 * @param activityName
	 *            The name of the activity
	 * @param period
	 *            The TimePeripod the activity should have
	 * @throws DoesNotExistException
	 *             If the activity does not exist
	 */
	public void adminSetActivityTimePeriod(String activityName, TimePeriod period) throws DoesNotExistException {
		appInfo.getActivity(activityName).setTimePeriod(period);
	}

	/**
	 * Gets the TimePeriod of the activity with name activityName
	 *
	 * @throws DoesNotExistException
	 *             if the activity does not exist
	 * @param activityName
	 *            The name of the activity
	 * @return TimePeriod of the activity
	 */
	public TimePeriod getActivityTimePeriod(String activityName) throws DoesNotExistException {
		return appInfo.getActivity(activityName).getTimePeriod();
	}
}
