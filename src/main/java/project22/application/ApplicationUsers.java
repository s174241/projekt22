package project22.application;

import java.time.LocalDateTime;
import java.util.List;

import project22.domain.Developer;
import project22.domain.ProjectLeader;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;

/**
 *
 * @author Magnus
 *
 */
public class ApplicationUsers {

	private ApplicationInfo appInfo;

	public ApplicationUsers(ApplicationInfo appInfo) {
		this.appInfo = appInfo;
	}

	/**
	 * Sets the current user to the user with the specified initials.
	 *
	 * @throws DoesNotExistException
	 *             if there's not registered a user with those initials
	 * @param initials
	 *            the initials of the user specified when registering
	 */
	public void setCurrentUser(String initials) throws DoesNotExistException {
		appInfo.currentUser = appInfo.getUser(initials);
	}

	/**
	 * Ensures that the given initials are free to use; throws an exception if
	 * they are not.
	 *
	 * @throws AlreadyExistsException
	 *             if the user is already registered
	 * @param initials
	 *            The initials of the user specified when registering
	 */
	private void ensureNotRegistered(String initials) throws AlreadyExistsException {
		if (appInfo.isRegistered(initials))
			throw new AlreadyExistsException("An employee with those initials is already registered in the company");
	}

	/**
	 * Registers the user in the company if the given initials are not already used.
	 *
	 * @throws AlreadyExistsException
	 *             if a user is already registered
	 * @param initials
	 *            the initials of the user specified when registering
	 */
	public void registerDeveloper(String initials) throws AlreadyExistsException {
		ensureNotRegistered(initials);
		appInfo.company.registerEmployee(new Developer(initials));
	}

	/**
	 * Registers the developer with the company if the given initials are not already used.
	 *
	 * @throws AlreadyExistException
	 *             if the user is already registered
	 * @param initials
	 *            the initials of the user specified when registering
	 * @param date
	 *            the date of the creation of the user
	 */
	public void registerDeveloper(String initials, LocalDateTime date) throws AlreadyExistsException {
		ensureNotRegistered(initials);
		appInfo.company.registerEmployee(new Developer(initials, date));
	}

	/**
	 * Register the project leader in the project if the given initials are not already
	 * used.
	 *
	 * @throws AlreadyExistException
	 *             if a user with those initials is already registered
	 * @param initials
	 *            the initials of the user specified when registering
	 */
	public void registerProjectLeader(String initials) throws AlreadyExistsException {
		ensureNotRegistered(initials);
		appInfo.company.registerEmployee(new ProjectLeader(initials));
	}

	/**
	 * Get a list of the names of the activities the user with those initials is
	 * working on.
	 *
	 * @return a list of the names of the activities the user with initials initials
	 *         is working on
	 * @param initials
	 *            the initials of the user specified when registering
	 */
	public List<String> getActivitiesOfWorker(String initials) {
		return appInfo.company.getEmployee(initials).getActivityNames();
	}

	/**
	 * Gets the date the user with those initials was created.
	 *
	 * @return the date the user with initials initials was created
	 * @throws DoesNotExistException
	 *             if the user does not exist
	 */
	public LocalDateTime getUserCreationDate(String initials) throws DoesNotExistException {
		return appInfo.getUser(initials).getCreationDate();
	}
}
