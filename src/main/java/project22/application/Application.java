package project22.application;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import project22.domain.Company;

/**
 *
 * @author Magnus
 *
 */
public class Application {

	public ApplicationInfo appInfo;
	public ApplicationActivity appActivity;
	public ApplicationProjects appProjects;
	public ApplicationUsers appUsers;
	public ApplicationTimeRegistration appTimeRegistry;

	public Application() {
		appInfo = new ApplicationInfo(new Company());
		appActivity = new ApplicationActivity(appInfo);
		appProjects = new ApplicationProjects(appInfo);
		appUsers = new ApplicationUsers(appInfo);
		appTimeRegistry = new ApplicationTimeRegistration(appInfo);
	}

	public static LocalDateTime stringToLocalDateTime(String date) {
		return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
	}
}
