package project22.application;

import java.util.List;
import java.util.stream.Collectors;

import project22.domain.Project;
import project22.domain.ProjectLeader;
import project22.domain.TimePeriod;
import project22.domain.User;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;
import project22.exceptions.UnauthorizedException;

/**
 *
 * @author Adam
 *
 */
public class ApplicationProjects {

	private ApplicationInfo appInfo;

	public ApplicationProjects(ApplicationInfo appInfo) {
		this.appInfo = appInfo;
	}

	/**
	 * Opens a project with id in Application Info to be used in the future.
	 *
	 * @throws DoesNotExistException
	 *             if the project does not exist
	 * @param id
	 *            The id of the project
	 */
	public void openProject(int id) throws DoesNotExistException {
		Project project = appInfo.company.getProject(id);
		appInfo.openProject = project;
	}

	/**
	 * Creates a project without any restrictions.
	 * WARNING: SHOULD ONLY BE USED WHEN TESTING
	 *
	 * @throws ExceededLimitException
	 *             if too many projects exist and another cannot be created
	 * @return the project id
	 */
	public int adminCreateProject() throws ExceededLimitException {
		return appInfo.company.registerProject();
	}

	/**
	 * Creates a new project.
	 *
	 * @throws UnauthorizedException
	 *             if the user is not a project leader
	 * @throws ExceededLimitException
	 *             if too many projects exist and another cannot be created
	 */
	public int createProject() throws UnauthorizedException, ExceededLimitException {
		appInfo.ensureProjectLeader(appInfo.currentUser, "You need to be a project leader to create a project");
		return adminCreateProject();
	}

	/**
	 * Changes the name of the project if it exist and the current user is a project
	 * leader.
	 *
	 * @throws DoesNotExistException
	 *             if the project does not exist
	 * @throws UnautorizedException
	 *             if the current user is not a project leader
	 * @param name
	 *            The new name of the project
	 */
	public void setProjectName(String name) throws DoesNotExistException, UnauthorizedException {
		appInfo.ensureOpenProject();
		appInfo.ensureProjectLeader("You need to be project leader to change the name of a project");
		appInfo.openProject.setName(name);
	}

	/**
	 * Gets a list of all the names of the projects in the company.
	 *
	 * @return a list of all the names of all the projects in the company
	 */
	public List<String> getProjectNames() {
		return appInfo.company.getProjects().stream()
				.map(project -> project.getName())
				.collect(Collectors.toList());
	}

	/**
	 * Get the budgeted time of the open project.
	 *
	 * @return the budgeted time of the open project
	 * @throws DoesNotExistException
	 *             if the project does not exist
	 */
	public int getBudgetTime() throws DoesNotExistException {
		appInfo.ensureOpenProject();
		return appInfo.openProject.getBudgetedTime();
	}

	/**
	 * Sets the time period of the open project.
	 *
	 * @throws DoesNotExistException
	 *             if the project does not exist
	 * @throws UnauthorizedException
	 *             if the current user is not a project leader
	 * @param period
	 *            The TimePeriod the project should have
	 */
	public void setProjectTimePeriod(TimePeriod period) throws DoesNotExistException, UnauthorizedException {
		appInfo.ensureOpenProject();
		appInfo.ensureProjectLeader("Only project leaders can edit time periods");
		appInfo.openProject.setTimePeriod(period);
	}

	/**
	 * Gets the TimePeriod of the project.
	 *
	 * @return the TimePeriod of the project
	 * @throws DoesNotExistException
	 *             if the project does not exist
	 */
	public TimePeriod getTimePeriod() throws DoesNotExistException {
		appInfo.ensureOpenProject();
		return appInfo.openProject.getTimePeriod();
	}

	/**
	 * Gets a list of the initials of all the workers woking on the project, with no
	 * duplicates.
	 *
	 * @return a list of the initials of the workers of the project
	 * @throws DoesNotExistException
	 *             if the project does not exist
	 */
	public List<String> getWorkersOfProject() throws DoesNotExistException {
		appInfo.ensureOpenProject();
		return appInfo.openProject.findDeveleopersOfProject().stream().map(user -> user.getInitials())
				.collect(Collectors.toList());
	}

	/**
	 * Gets the initials of the project leader of the open project.
	 *
	 * @return the initials of the project leader of the project
	 * @throws DoesNotExistException
	 * 				if the project does not exist
	 */
	public String getProjectLeader() throws DoesNotExistException {
		appInfo.ensureOpenProject();
		ProjectLeader projectLeader = appInfo.openProject.getProjectLeader();
		if(projectLeader == null)
			throw new DoesNotExistException("Project leader not specified");
		return projectLeader.getInitials();
	}

	/**
	 * Assigns the project leader of the project to the user with initials initials.
	 *
	 * @param initials
	 *            The initials of the user to be assigned project leader
	 * @throws DoesNotExistException
	 *             if no project is open
	 * @throws UnauthorizedException
	 *             if the current user is no project leader
	 */
	public void assignProjectLeader(String initials) throws DoesNotExistException, UnauthorizedException {
		appInfo.ensureOpenProject();
		appInfo.ensureProjectLeader("You need to be a project leader");
		User user = appInfo.getUser(initials);
		if (!appInfo.isProjectLeader(user))
			throw new UnauthorizedException("A developer cannot be a project leader");
		appInfo.openProject.setProjectLeader((ProjectLeader) user);
	}

}
