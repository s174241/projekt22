package project22.application;

import java.util.List;
import java.util.stream.Collectors;

import project22.domain.Activity;
import project22.domain.Company;
import project22.domain.Project;
import project22.domain.TimePeriod;
import project22.domain.User;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.UnauthorizedException;

/**
 *
 * @author Magnus
 *
 */
public class ApplicationInfo {

	public Company company;
	public User currentUser;
	public Project openProject;

	public ApplicationInfo(Company company) {
		this.company = company;
	}

	/**
	 * Checks if the user is a project leader.
	 *
	 * @return true if the user is a project leader, false otherwise
	 * @param user
	 *            the user that is being tested
	 */
	public boolean isProjectLeader(User user) {
		if (user.getType().equals("project leader"))
			return true;
		return false;
	}

	/**
	 * Gets the current user.
	 *
	 * @return the current user
	 */
	public User getCurrentUser() {
		return currentUser;
	}

	/**
	 * Ensures that the <b>given user</b> is a project leader; throws an exception
	 * if that is not the case.
	 *
	 * @throws UnauthorizedException
	 *             if the user is not a project leader
	 * @param user
	 *            the user that is being tested
	 * @param exceptionMessage
	 *            the message the exception will hold if the user is not a project
	 *            leader
	 */
	public void ensureProjectLeader(User user, String exceptionMessage) throws UnauthorizedException {
		if (!isProjectLeader(user))
			throw new UnauthorizedException(exceptionMessage);
	}

	/**
	 * Ensures that the <b>current user</b> is a project leader; throws an exception
	 * if that is not the case.
	 *
	 * @throws UnauthorizedException
	 *             if the current user is not a project leader
	 * @param exceptionMessage
	 *            the message the exception will hold if the user is not a project
	 *            leader
	 */
	public void ensureProjectLeader(String exceptionMessage) throws UnauthorizedException {
		ensureProjectLeader(currentUser, exceptionMessage);
	}

	/**
	 * Checks if a user with those initials is registered with the company.
	 *
	 * @return true if a user with those initials is already registered, false
	 *         otherwise
	 * @param initials
	 *            the initials of the user specified when registering
	 */
	public boolean isRegistered(String initials) {
		if (company.getEmployee(initials) == null)
			return false;
		return true;
	}

	/**
	 * Checks if a project is open.
	 *
	 * @return true if an open project exists, false otherwise
	 */
	public boolean hasOpenProject() {
		if (openProject == null)
			return false;
		return true;
	}

	/**
	 * Get a user by their initials.
	 *
	 * @return The user with those initials.
	 * @throws DoesNotExistException
	 *             if no user is registered with the given initials
	 * @param initials
	 *            the initials to look up
	 */
	public User getUser(String initials) throws DoesNotExistException {
		User user = company.getEmployee(initials);
		if (user == null)
			throw new DoesNotExistException("User doesn't exist");
		return user;
	}

	/**
	 * Ensures a project is open; throws an exception if that is not the case.
	 *
	 * @throws DoesNotExistException
	 *             if the project does not exist
	 */
	public void ensureOpenProject() throws DoesNotExistException {
		if (!hasOpenProject())
			throw new DoesNotExistException("That project doesn't not exist");
	}

	/**
	 * Mark the project with the given id as open.
	 *
	 * @param id
	 *            the id of the project to open
	 */
	public void openProject(int id) {
		openProject = company.getProject(id);
	}

	/**
	 * Find an activity by name.
	 * Searches in the currently opened project.
	 *
	 * @return an activity with the given name
	 * @throws DoesNotExistException
	 *             if no activity has that name or no project is open
	 * @param activityName
	 *            the name of the activity
	 */
	public Activity getActivity(String activityName) throws DoesNotExistException {
		ensureOpenProject();
		Activity activity = openProject.getActivity(activityName);
		if (activity == null)
			throw new DoesNotExistException("Unable to find activity to edit");
		return activity;
	}

	/**
	 * Gets a list of all the employees registered in the company.
	 *
	 * @return the list of the employees initials
	 */
	public List<String> getAllEmployees() {
		return company.getEmployees().stream()
				.map(employee -> employee.getInitials())
				.collect(Collectors.toList());
	}

	/**
	 * Computes the amount of time worked on the currently open project in the given period.
	 *
	 * @param period
	 *            the TimePeriod in which to compute hours of work
	 * @return the amount of time worked
	 */
	public int getHoursWorked(TimePeriod period) {
		return openProject.getHoursWorkedOn(period);
	}

}
