
package project22.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Adam
 *
 */
public class TimePeriod {
	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	private LocalDateTime startDate;
	private LocalDateTime endDate;

	public TimePeriod(LocalDateTime startDate, LocalDateTime endDate) {
		assert startDate.isBefore(endDate);
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public TimePeriod(String start, String end) {
		this(toLocalDateTime(start), toLocalDateTime(end));
	}

	public TimePeriod(LocalDate from, LocalDate to) {
		this(LocalDateTime.of(from, LocalTime.MIN),LocalDateTime.of(to, LocalTime.MAX));
	}

	private static LocalDateTime toLocalDateTime(String string) {
		return LocalDateTime.parse(string, formatter);
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public boolean overlaps(TimePeriod period) {
		return overlaps(period.getStartDate(), period.getEndDate());
	}

	public boolean overlaps(LocalDateTime start, LocalDateTime end) {
		assert start.isBefore(end);
		return !(
			this.endDate.isBefore(start) /* 1 */ ||
			this.startDate.isAfter(end) /* 2 */
		);
	}
	
	public boolean contains(LocalDateTime date) {
		return this.startDate.isBefore(date) && this.endDate.isAfter(date);
	}
	
	public boolean contains(TimePeriod period) {
		return this.contains(period.startDate) && this.contains(period.endDate);
	}
	
	public boolean isContainedIn(TimePeriod period) {
		return period.contains(this.startDate) && period.contains(this.endDate);
	}

	public boolean equals(Object other) {
		TimePeriod period = (TimePeriod) other;
		return period.getStartDate().isEqual(this.getStartDate()) &&
				period.getEndDate().isEqual(this.getEndDate());
	}

	public String toString() {
		return startDate.toLocalDate().toString() + " - " + endDate.toLocalDate().toString();
	}
}
