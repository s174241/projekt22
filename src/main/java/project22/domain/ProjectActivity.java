package project22.domain;

/**
 *
 * @author Adam
 *
 */
public class ProjectActivity extends Activity{
	public ProjectActivity(String name) {
		super(name);
	}

	@Override
	public boolean getExclusivity() {
		return false;
	}
}
