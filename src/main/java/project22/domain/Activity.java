package project22.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * An activity is used for registering hours for an employee. A list of users
 * can be assigned to the activity, but users not assigned are also allowed to
 * register hours to it.
 *
 * @version 1.0
 * @author Adam
 */
public abstract class Activity {
	private String name;
	private int budgetedTime;
	private List<User> users;
	private TimePeriod period;
	public List<TimeRegistration> timeRegistry;

	/**
	 * Creates a new activity with an empty list of users and an empty time
	 * registry.
	 *
	 * @param name
	 *            Name of the activity being created.
	 */
	public Activity(String name) {
		this(name, new ArrayList<User>());
	}

	/**
	 * Creates a new activity with the given users and an empty time registry.
	 *
	 * @param name
	 *            Name of the activity being created.
	 * @param users
	 *            List of users to be in the activity.
	 */
	public Activity(String name, List<User> users) {
		this(name, users, new ArrayList<TimeRegistration>());
	}

	/**
	 * Creates a new activity with the given users and time registrations.
	 *
	 * @param name
	 *            Name of the activity being created.
	 * @param users
	 *            List of users to be in the activity.
	 * @param timeRegistry
	 *            List of time registrations to be in the activity.
	 */
	public Activity(String name, List<User> users, List<TimeRegistration> timeRegistry) {
		this.name = name;
		this.users = users;
		this.timeRegistry = timeRegistry;
		this.budgetedTime = 0;
	}

	/**
	 * Check if the activity is exclusive.
	 *
	 * If an activity is exclusive, any developer assigned to it is considered
	 * unavailable in the activities' time period.
	 *
	 * @return True if the activity is exclusive, false otherwise.
	 */
	public abstract boolean getExclusivity();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return users;
	}

	/**
	 * Adds a user to the list of developers on the activity.
	 *
	 * @param user
	 *            The user to be added to the activity.
	 */
	public void addUser(User user) {
		users.add(user);
		user.addActivity(this);
	}

	/**
	 * Removes a user from the list of developers on the activity.
	 *
	 * @param user
	 *            The user to be removed from the activity.
	 */
	public void removeUser(User user) {
		users.remove(user);
		user.removeActivity(this);
	}
	
	public List<TimeRegistration> getRegistry() {
		return this.timeRegistry;
	}

	public int getBudgetedTime() {
		return budgetedTime;
	}

	public void setBudgetedTime(int budgetedTime) {
		this.budgetedTime = budgetedTime;
	}

	public TimePeriod getTimePeriod() {
		return period;
	}

	public void setTimePeriod(TimePeriod period) {
		this.period = period;
	}

	/**
	 * Register time spent working on the activity.
	 *
	 * When an employee registers hours on the activity, it is added to the
	 * registration list.
	 *
	 * @param registration
	 *            The registration that is added.
	 */
	public void addTimeRegistration(TimeRegistration registration) {
		timeRegistry.add(registration);
	}

	/**
	 * Remove time spent working on the activity.
	 *
	 * @param user
	 *            The user to remove a time registration for.
	 * @param date
	 *            The date the hours should was registered on.
	 */
	public boolean removeTimeRegistration(User user, LocalDateTime date) {
		Optional<TimeRegistration> time = timeRegistry.stream()
				.filter(t -> t.getWorkDate().equals(date))
				.filter(t -> t.getWorker().equals(user))
				.findFirst();
		if(time.isPresent()){
			timeRegistry.remove(time.get());
			return true;
		}else
			return false;
	}

	/**
	 * Computes the amount of hours a given user has registered on a given date.
	 *
	 * @param user
	 *            The employee you wish to find the hours worked for
	 * @param date
	 *            The date you wish to check hours worked on
	 * @return The amount of hours the user has worked on the given date
	 */
	public int getHoursWorkedOn(User user, LocalDateTime date) {
		assert user != null;
		assert date != null;

		int res = timeRegistry.stream()
				.filter(t -> t.getWorkDate().equals(date))
				.filter(t -> t.getWorker().equals(user))
				.mapToInt(t -> t.getTimeSpent())
				.sum();

		assert res >= 0;
		return res;
	}

	/**
	 * Computes the total amount of hours worked on the activity.
	 *
	 * @return The total amount hours worked on the activity
	 */
	public int getHoursWorkedOn() {
		return timeRegistry.stream()
				.mapToInt(t -> t.getTimeSpent())
				.sum();
	}

	/**
	 * Computes the total amount of hours a given worker has worked on the activity.
	 *
	 * @param user
	 *            The user you wish to find the hours worked for
	 * @return The total amount of hours the user has registered on the activity
	 */
	public int getHoursRegistered(User user) {
		return timeRegistry.stream()
				.filter(t -> t.getWorker().equals(user))
				.mapToInt(t -> t.getTimeSpent())
				.sum();
	}

	/**
	 * Computes the total amount of hours registered on the activity between two
	 * dates.
	 *
	 * @param startDate
	 *            The beginning of the period
	 * @param endDate
	 *            The end of the period
	 * @return The the amount of hours registered on the activity between startDate
	 *         and endDate.
	 */
	public int getHoursWorkedOn(LocalDateTime startDate, LocalDateTime endDate) {
		return timeRegistry.stream()
				.filter(t -> startDate.isBefore(t.getWorkDate()))
				.filter(t -> endDate.isAfter(t.getWorkDate()))
				.mapToInt(t -> t.getTimeSpent())
				.sum();
	}


}
