package project22.domain;
/**
 *
 * @author Jenath
 *
 */
public class ProjectLeader extends User {
	public ProjectLeader(String initials) {
		super(initials);
	}

	@Override
	public String getType() {
		return "project leader";
	}
}
