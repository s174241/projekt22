package project22.domain;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author Jenath
 *
 */
public class TimeRegistration {
	private TimePeriod period;
	private User worker;

	public TimeRegistration(LocalDateTime date, int timeSpent, User worker) {
		this(new TimePeriod(
				date,
				date.plusHours(timeSpent)
		), worker);
	}

	public TimeRegistration(TimePeriod period, User user) {
		this.period = period;
		this.worker = user;
	}

	public LocalDateTime getWorkDate() {

		return getPeriod().getStartDate();
	}
	
	public TimePeriod getPeriod() {
		return this.period;
	}

	public int getTimeSpent() {
		return (int) ChronoUnit.HOURS.between(
			period.getStartDate(),
			period.getEndDate()
		);
	}

	public User getWorker() {
		return worker;
	}
}
