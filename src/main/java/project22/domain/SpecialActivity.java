
package project22.domain;

/**
 *
 * @author Magnus
 *
 */
public class SpecialActivity extends Activity {

	public enum Type {
		SICK_LEAVE("Sick leave"),
		VACATION("Vacation"),
		WORK_RELATED("Work related");

		private final String text;

		Type(final String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	private Type type;

	public SpecialActivity(Type type, TimePeriod period) {
		super(type.toString() + period.toString());
		this.setTimePeriod(period);
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	@Override
	public boolean getExclusivity() {
		return true;
	}


}
