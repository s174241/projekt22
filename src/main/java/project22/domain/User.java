package project22.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
/**
 *
 * @author Jenath
 *
 */
public abstract class User {
	private String initials;
	private List<Activity> activities;
	private LocalDateTime creationDate;

	public User(String initials, LocalDateTime date) {
		this.initials = initials;
		this.activities = new ArrayList<Activity>();
		this.creationDate = date;
	}

	public User(String initials) {
		this(initials, LocalDateTime.now());
	}

	public String getInitials() {
		return initials;
	}

	public boolean addActivity(Activity activity) {
		return activities.add(activity);
	}

	public boolean removeActivity(Activity activity) {
		return activities.remove(activity);
	}

	public List<Activity> getActivities() {
		return activities;
	}

	public List<String> getActivityNames() {
		return getActivities().stream()
				.map(activity -> activity.getName())
				.collect(Collectors.toList());
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	/**
	 * The type of user.
	 * A user can either be a developer or a project leader.
	 *
	 * @return the type of the user
	 */
	public abstract String getType();

	public boolean isAvailable(TimePeriod period) {
		if (period == null) {
			return false;
		}
		// All activities that overlap
		Supplier<Stream<Activity>> supplier = () -> activities.stream()
				.filter(activity -> activity.getTimePeriod() != null)
				.filter(activity -> activity.getTimePeriod().overlaps(period));
		// A developer is available if
		// 1) less than 20 overlapping activities exist
		// 2) none of the overlapping activities are exclusive
		return supplier.get().count() < 20 && supplier.get()
				.noneMatch(activity -> activity.getExclusivity());
	}

	public Activity getSpecialActivityInPeriod(TimePeriod period) {
		return activities.stream()
					.filter(activity -> activity instanceof SpecialActivity)
					.filter(activity -> activity.getTimePeriod().equals(period))
					.findFirst()
					.orElse(null);
	}
}
