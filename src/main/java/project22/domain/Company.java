package project22.domain;

import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import project22.exceptions.ExceededLimitException;

/**
 *
 * @author Adam
 *
 */
public class Company {
	private Map<String, User> employees;
	private Map<Integer, Project> projects;
	// bruges til at lave UUIDs
	private int idCounter = 0;
	// currentYear holder de sidste to cifre i årstallet.
	private int currentYear = Year.now().getValue() % 100;

	public Company() {
		this.employees = new HashMap<>();
		this.projects = new HashMap<>();
	}

	private int findAvailableId() {
		int id = currentYear * 10000 + idCounter;
		idCounter++;
		return id;
	}

//	private int findAvailableId() throws ExceededLimitException {
//		// Hvis der har været årsskifte, sættes idCounter til 0, og currentYear
//		// opdateres
//		if (Year.now().getValue() % 100 != currentYear) {
//			currentYear = Calendar.getInstance().get(Calendar.YEAR) % 100;
//			idCounter = 0;
//		}
//
//		idCounter++;
//
//		// Giver fejl og returnerer -1, hvis der ikke er flere ID tilbage for dette år.
//		if (idCounter >= 10000) {
//			throw new ExceededLimitException("Too many projects, cannot create more until next year");
//		}
//
//		// Returnerer id YYXXXX hvor YY er år, og XXXX er løbenummer (idCounter).
//		return currentYear * 10000 + idCounter;
//	}

	public void registerEmployee(User user) {
		assert user != null;
		employees.put(user.getInitials(), user);
		assert user.equals(getEmployee(user.getInitials()));
	}

	public User getEmployee(String initials) {
		return employees.get(initials);
	}

	public List<User> getEmployees() {
		return new ArrayList<User>(employees.values());
	}

	public List<User> getAvailableDevelopers(TimePeriod period) {
		return employees.values().stream()
					.filter(developer -> developer.isAvailable(period))
					.collect(Collectors.toList());
	}

	public Project getProject(int projectId) {
		return projects.get(projectId);
	}

	public List<Project> getProjects() {
		return new ArrayList<Project>(projects.values());
	}

	public int registerProject() throws ExceededLimitException {
		int id = findAvailableId();
		projects.put(id, new StandardProject(id));
		return id;
	}
}
