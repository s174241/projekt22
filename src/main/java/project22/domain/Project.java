package project22.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import project22.exceptions.AlreadyExistsException;
/**
 *
 * @author Philip
 *
 */
public abstract class Project {
	private final int id;
	private String name;
	public List<Activity> activities;
	private ProjectLeader projectLeader;
	private TimePeriod period;

	protected Project(int id) {
		this.id = id;
		this.activities = new ArrayList<Activity>();
		this.name = "";
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProjectLeader getProjectLeader() {
		return projectLeader;
	}

	public void setProjectLeader(ProjectLeader projectLeader) {
		this.projectLeader = projectLeader;
	}

	public int getBudgetedTime() {
		return activities.stream()
				.mapToInt(a -> a.getBudgetedTime())
				.sum();
	}

	public TimePeriod getTimePeriod() {
		return period;
	}

	public void setTimePeriod(TimePeriod period) {
		this.period = period;
	}

	public List<Activity> getActivities() {
		return this.activities;
	}

	public void addActivity(Activity activity) throws AlreadyExistsException {
		assert activity != null;
		assert !(activity instanceof SpecialActivity);

		if (activities.stream().anyMatch(a -> a.getName().equals(activity.getName()))) {
			throw new AlreadyExistsException("Activities must have unique names");
		}
		activities.add(activity);
	}

	public List<User> findDeveleopersOfProject() {
		return activities.stream()
				.map(activity -> activity.getUsers())
				.flatMap(users -> users.stream())
				.distinct()
				.collect(Collectors.toList());
	}

	public int getHoursWorkedOn(TimePeriod period) {
		return getHoursWorkedOn(period.getStartDate(), period.getEndDate());
	}

	public int getHoursWorkedOn(LocalDateTime start, LocalDateTime end) {
		return activities.stream()
				.mapToInt(activity -> activity.getHoursWorkedOn(start, end))
				.sum();
	}

	public Activity getActivity(String name) {
		return activities.stream()
				.filter(activity -> activity.getName().equals(name))
				.findFirst()
				.orElse(null);
	}
}
