package project22.domain;

import java.time.LocalDateTime;

/**
 *
 * @author Jenath
 *
 */
public class Developer extends User{
	public Developer(String initials) {
		super(initials);
	}

	public Developer(String initials, LocalDateTime date) {
		super(initials, date);
	}

	@Override
	public String getType() {
		return "developer";
	}
}
