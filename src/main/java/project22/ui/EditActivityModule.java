package project22.ui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import project22.domain.Activity;
import project22.domain.TimePeriod;
import project22.domain.User;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.UnauthorizedException;
import project22.ui.helpers.AlertHelper;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.NumericField;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;

public class EditActivityModule implements Module<View, Controller> {
	private View view;
	private Controller controller;

	public EditActivityModule() {
		this.view = new EditActivityView();
		this.controller = new EditActivityController();
	}

	@Override
	public View getView() {
		return this.view;
	}

	@Override
	public Controller getController() {
		return this.controller;
	}
}
/**
 *
 * @author Philip
 *
 */
class EditActivityView implements View {
	private Map<Button, User> assignedDevsMap = new HashMap<>();
	private Map<Button, User> availableDevsMap = new HashMap<>();
	private DatePicker fromPicker;
	private DatePicker toPicker;
	private TextField nameField;
	private NumericField budgetedField;
	private Button saveButton;

	public Node render(ReadableStore store) {
		Activity activity = store.getActivity();

		VBox vbox = new VBox(10);

		Label nameLabel = new Label("Name");
		nameLabel.setFont(new Font("Roboto", 20));
		nameField = new TextField(activity.getName());

		Label periodLabel = new Label("Time period");
		periodLabel.setFont(new Font("Roboto", 20));
		HBox dateBox = new HBox(10);
		Label between = new Label("to");
		fromPicker = new DatePicker();
		toPicker = new DatePicker();
		try {
			TimePeriod period = store.getActivityTimePeriod();
			if (period != null) {
				fromPicker.setValue(period.getStartDate().toLocalDate());
				toPicker.setValue(period.getEndDate().toLocalDate());
			}
		} catch (DoesNotExistException e2) {}
		dateBox.getChildren().addAll(fromPicker, between, toPicker);

		VBox budgetBox = new VBox(10);

		Label budgetedTimeLabel = new Label("Budgeted Time: ");
		budgetedTimeLabel.setFont(new Font("Roboto", 20));
		int budgetedTime = 0;
		try {
			budgetedTime = store.getActivityBudgetedTime();
		} catch (DoesNotExistException e1) {
			e1.printStackTrace();
		}

		budgetedField = new NumericField(budgetedTime);
		budgetedField.setText(""+budgetedTime);
		budgetBox.getChildren().addAll(budgetedTimeLabel, budgetedField);

		VBox timeBox = new VBox(10);
		Label timeWorkedLabel = new Label();
		timeWorkedLabel.setFont(new Font("Roboto", 20));
		try {
			timeWorkedLabel.setText("Work so far: " + store.getWorkedOnActivity());
		} catch (DoesNotExistException e1) {
			e1.printStackTrace();
			timeWorkedLabel.setText("Time spent: 0");
		}
		timeBox.getChildren().add(timeWorkedLabel);

		Label assignedDevsLabel = new Label("Assigned developers");
		assignedDevsLabel.setFont(new Font("Roboto", 20));
		final List<User> assignedDevs = new ArrayList<>();
		try {
			assignedDevs.addAll(store.getDevelopers());
		} catch (DoesNotExistException e) {
			AlertHelper.alertException(e);
		}
		VBox assignedDevsList = createAssignedList(assignedDevs);

		Label availableDevsLabel = new Label("Available developers");
		availableDevsLabel.setFont(new Font("Roboto", 20));
		List<User> availableDevs = store.getAvailableDevelopers(activity.getTimePeriod())
				.stream()
				.filter(dev -> !assignedDevs.contains(dev))
				.collect(Collectors.toList());
		VBox availableDevsList = createAvailableList(availableDevs);

		saveButton = new Button("Save");

		vbox.getChildren().addAll(
			nameLabel,
			nameField,
			periodLabel,
			dateBox,
			budgetBox,
			timeBox,
			assignedDevsLabel,
			assignedDevsList,
			availableDevsLabel,
			availableDevsList,
			saveButton
		);
		return vbox;
	}

	private VBox createAssignedListItem(User developer) {
		Label name = new Label(developer.getInitials());
		name.setFont(new Font("Roboto", 16));
		Button button = new Button("Remove");
		assignedDevsMap.put(button, developer);
		button.setPrefWidth(200);
		VBox box = new VBox(10);
		box.setPadding(new Insets(10, 10, 10, 10));
		box.setStyle("-fx-background-color: #dddddd");
		box.getChildren().addAll(name, button);
		return box;
	}

	private VBox createAvailableListItem(User developer) {
		Label name = new Label(developer.getInitials());
		name.setFont(new Font("Roboto", 16));
		Button button = new Button("Add");
		availableDevsMap.put(button, developer);
		button.setPrefWidth(200);
		VBox box = new VBox(10);
		box.setPadding(new Insets(10, 10, 10, 10));
		box.setStyle("-fx-background-color: #dddddd");
		box.getChildren().addAll(name, button);
		return box;
	}

	private VBox createAssignedList(List<User> developers) {
		VBox box = new VBox(10);
		for (User developer: developers) {
			VBox node = createAssignedListItem(developer);
			box.getChildren().add(node);
		}
		return box;
	}


	private VBox createAvailableList(List<User> developers) {
		VBox box = new VBox(10);
		for (User developer: developers) {
			VBox node = createAvailableListItem(developer);
			box.getChildren().add(node);
		}
		return box;
	}

	public String getName() {
		return this.nameField.getText();
	}

	public LocalDate getFromDate() {
		return this.fromPicker.getValue();
	}

	public LocalDate getToDate() {
		return this.toPicker.getValue();
	}

	public int getBudgetedTime() {
		return this.budgetedField.getValue();
	}

	public void onRemove(Consumer<User> handler) {
		Set<Button> buttons = assignedDevsMap.keySet();
		for (Button button : buttons) {
			User dev = assignedDevsMap.get(button);
			button.setOnAction(ev -> {
				handler.accept(dev);
			});
		}
	}

	public void onAdd(Consumer<User> handler) {
		Set<Button> buttons = availableDevsMap.keySet();
		for (Button button : buttons) {
			User dev = availableDevsMap.get(button);
			button.setOnAction(ev -> {
				handler.accept(dev);
			});
		}
	}

	public void onSave(Runnable handler) {
		saveButton.setOnAction(ev -> {
			handler.run();
		});
	}
}
/**
 *
 * @author Magnus
 *
 */
class EditActivityController implements Controller {
	@Override
	public void connected(ControllingContext context) {
		EditActivityView view = (EditActivityView) context.getView();
		Store store = context.getStore();

		String originalName = store.getActivity().getName();
		TimePeriod checkperiod = null;
		try {
			checkperiod = store.getActivityTimePeriod();
		} catch (DoesNotExistException e) {
		}
		TimePeriod originalPeriod = checkperiod;

		int checkBudget = 0;
		try {
			checkBudget = store.getActivityBudgetedTime();
		} catch (DoesNotExistException e1) {}
		int originalBudgetTime = checkBudget;

		view.onRemove(dev -> {
			try {
				store.removeDeveloper(dev.getInitials());
				context.connect(new EditActivityModule(),false);
			} catch (UnauthorizedException | DoesNotExistException e) {
				AlertHelper.alertException(e);
			}

		});

		view.onAdd(dev -> {
			try {
				store.addDeveloper(dev.getInitials());
				context.connect(new EditActivityModule(),false);
			} catch (DoesNotExistException | UnauthorizedException e) {
				AlertHelper.alertException(e);
			}
		});

		view.onSave(() -> {
			String name = view.getName();
			LocalDate from = view.getFromDate();
			LocalDate to = view.getToDate();
			int budgetTime = view.getBudgetedTime();

			if (from != null && to != null) {
				TimePeriod period = new TimePeriod(from, to);
				if (originalPeriod == null || !originalPeriod.equals(period)) {
					try {
						store.setActivityTimePeriod(period);
					} catch (UnauthorizedException | DoesNotExistException e) {
						AlertHelper.alertException(e);
					}
				}
			}

			if (budgetTime != originalBudgetTime) {
				try {
					store.setActivityBudgetedTime(budgetTime);
				} catch (UnauthorizedException | DoesNotExistException e) {
					AlertHelper.alertException(e);
				}
			}

			if (!originalName.equals(name)) {
				try {
					store.setActivityName(name);
				} catch (UnauthorizedException | DoesNotExistException e) {
					AlertHelper.alertException(e);
				}
			}

			context.connect(new EditProjectModule());
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		EditActivityView view = (EditActivityView) context.getView();
		view.onRemove(null);
		view.onAdd(null);
		view.onSave(null);
	}
}