package project22.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.Pair;
import project22.exceptions.DoesNotExistException;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;

public class DashboardModule implements Module<View, Controller> {
	private View view;
	private Controller controller;

	public DashboardModule() {
		this.view = new DashboardView();
		this.controller = new DashboardController();
	}

	@Override
	public View getView() {
		return this.view;
	}

	@Override
	public Controller getController() {
		return this.controller;
	}
}
/**
 *
 * @author Philip
 *
 */
class DashboardView implements View {
	private Map<Button, Pair<Integer, String>> projects = new HashMap<>();

	public Node render(ReadableStore store) {
		VBox vbox = new VBox(10);

		Label currentUser = new Label("Current user: " + store.getCurrentUser().getInitials());
		currentUser.setFont(new Font("Roboto", 20));

		Label activeProjectsLabel = new Label("My Projects");
		activeProjectsLabel.setFont(new Font("Roboto", 20));

		VBox activeProjectsList = createList(store.getUserProjects());

		Label allProjectsLabel = new Label("All Projects");
		allProjectsLabel.setFont(new Font("Roboto", 20));

		VBox allProjectsList = createList(store.getAllProjects());

		vbox.getChildren().addAll(
			currentUser,
			activeProjectsLabel,
			activeProjectsList,
			allProjectsLabel,
			allProjectsList
		);
		return vbox;
	}

	private VBox createListItem(Pair<Integer, String> project) {
		Label name = new Label(project.getKey() + " : " + project.getValue());
		name.setFont(new Font("Roboto", 16));
		Button button = new Button("View");
		projects.put(button, project);
		button.setPrefWidth(200);
		VBox box = new VBox(10);
		box.setPadding(new Insets(10, 10, 10, 10));
		box.setStyle("-fx-background-color: #dddddd");
		box.getChildren().addAll(name, button);
		return box;
	}

	private VBox createList(List<Pair<Integer,String>> projects) {
		VBox box = new VBox(10);
		for (Pair<Integer, String> project: projects) {
			VBox node = createListItem(project);
			box.getChildren().add(node);
		}
		return box;
	}

	public void bindButtons(Consumer<Integer> handler) {
		Set<Button> buttons = projects.keySet();
		for (Button button: buttons) {
			Pair<Integer, String> project = projects.get(button);
			button.setOnAction(ev -> {
				handler.accept(project.getKey());
			});
		}
	}
}
/**
 *
 * @author Magnus
 *
 */
class DashboardController implements Controller {
	@Override
	public void connected(ControllingContext context) {
		DashboardView view = (DashboardView) context.getView();
		view.bindButtons(project -> {
			try {
				context.getStore().openProject(project);
				context.connect(new EditProjectModule());
			} catch (NumberFormatException | DoesNotExistException e) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle(e.getMessage());
				alert.setHeaderText(null);
				alert.setContentText(e.getMessage());
				alert.show();
			}
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		DashboardView view = (DashboardView) context.getView();
		view.bindButtons(null);
	}
}