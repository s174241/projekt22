package project22.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;
/**
 *
 * @author Philip
 *
 */
public class MenuModule implements Module<View, Controller> {
	private View view;
	private Controller controller;

	public MenuModule() {
		this.view = new MenuView();
		this.controller = new MenuController();
	}

	@Override
	public View getView() {
		return view;
	}

	@Override
	public Controller getController() {
		return controller;
	}
}

class MenuView implements View {
	private Button registerHoursButton;
	private Button manageRegistrationsButton;
	private Button createProjectButton;
	private Button manageProjectsButton;

	public Node render(ReadableStore store) {
		VBox vbox = new VBox(10);
		vbox.setAlignment(Pos.CENTER_LEFT);
		vbox.setPrefHeight(App.HEIGHT - 40);

		registerHoursButton = new Button("Register hours");
		registerHoursButton.setPrefWidth(200);
		
		manageRegistrationsButton = new Button("Manage registrations");
		manageRegistrationsButton.setPrefWidth(200);
		
		createProjectButton = new Button("Create new project");
		createProjectButton.setPrefWidth(200);

		manageProjectsButton = new Button("Manage projects");
		manageProjectsButton.setPrefWidth(200);


		vbox.getChildren().addAll(
			registerHoursButton,
			manageRegistrationsButton,
			createProjectButton,
			manageProjectsButton
		);
		return vbox;
	}

	protected void onRegister(EventHandler<ActionEvent> handler) {
		registerHoursButton.setOnAction(handler);
	}
	
	protected void onRegistry(EventHandler<ActionEvent> handler) {
		manageRegistrationsButton.setOnAction(handler);
	}

	protected void onManage(EventHandler<ActionEvent> handler) {
		manageProjectsButton.setOnAction(handler);
	}

	protected void onCreate(EventHandler<ActionEvent> handler) {
		createProjectButton.setOnAction(handler);
	}
}

class MenuController implements Controller {
	@Override
	public void connected(ControllingContext context) {
		MenuView view = (MenuView) context.getView();
		view.onRegister(event -> {
			context.connect(new RegisterHoursModule());
		});
		view.onManage(event -> {
			context.connect(new DashboardModule());
		});
		view.onCreate(event -> {
			context.connect(new CreateProjectModule());
		});
		view.onRegistry(event -> {
			context.connect(new EditHoursModule());
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		MenuView view = (MenuView) context.getView();
		view.onRegister(null);
		view.onManage(null);
		view.onCreate(null);
	}
}