package project22.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import project22.domain.Activity;
import project22.domain.TimeRegistration;
import project22.domain.User;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;

public class EditHoursModule implements Module<View, Controller> {
	private View view;
	private Controller controller;

	public EditHoursModule() {
		this.view = new EditHoursView();
		this.controller = new EditHoursController();
	}

	@Override
	public View getView() {
		return this.view;
	}

	@Override
	public Controller getController() {
		return this.controller;
	}
}

class EditHoursView implements View {
	private Map<Button, TimeRegistration> map = new HashMap<>();

	@Override
	public Node render(ReadableStore store) {
		User user = store.getCurrentUser();
		List<TimeRegistration> regs = store.getRegistrations();
				
		VBox box = new VBox(10);
		Label label = new Label("Registrations");
		label.setFont(new Font("Roboto", 20));
		box.getChildren().add(label);

		List<Activity> activities = store.getActivities();
		for (Activity activity: activities) {
			VBox actBox = new VBox(10);
			Label actName = new Label(activity.getName());
			actName.setFont(new Font("Roboto", 18));
			actBox.getChildren().addAll(actName);

			regs = activity.getRegistry()
					.stream()
					.filter(reg -> reg.getWorker().equals(user))
					.collect(Collectors.toList());

			for (TimeRegistration reg: regs) {
				VBox regBox = new VBox(10);
				Label regName = new Label(reg.getWorkDate().toString());
				Button btn = new Button("Fjern");
				map.put(btn, reg);
				regBox.getChildren().addAll(regName, btn);
				actBox.getChildren().add(regBox);
			}

			box.getChildren().add(actBox);
		}
		return box;
	}

	public void onRemove(Consumer<TimeRegistration> handler) {
		Set<Button> buttons = map.keySet();
		for (Button button : buttons) {
			TimeRegistration reg = map.get(button);
			button.setOnAction(ev -> {
				handler.accept(reg);
			});
		}
	}
}

class EditHoursController implements Controller {
	@Override
	public void connected(ControllingContext context) {
		EditHoursView view = (EditHoursView) context.getView();
		
		Store store = context.getStore();
		User user = store.getCurrentUser();

		view.onRemove(reg -> {
			store.removeRegistration(user, reg);
			context.connect(new EditHoursModule(), false);
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		EditHoursView view = (EditHoursView) context.getView();
		view.onRemove(null);
	}
}