package project22.ui;

import java.util.Optional;

import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import project22.exceptions.DoesNotExistException;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;

public class SignInModule implements  Module<View, Controller> {

	private View view;
	private Controller controller;

	public SignInModule() {
		this.view = new SignInView();
		this.controller = new SignInController();
	}

	public View getView() {
		return this.view;
	}

	public Controller getController() {
		return this.controller;
	}
}

/**
 *
 * @author Philip
 *
 */
class SignInView implements View {
	private TextField field;
	private Button signInButton;
	private Button registerButton;

	public Node render(ReadableStore store) {
		VBox vbox = new VBox(10);
		vbox.setAlignment(Pos.CENTER_LEFT);
		vbox.setPrefHeight(App.HEIGHT - 40);

		Label initialsLabel = new Label("Initials");
		field = new TextField();

		signInButton = new Button("Sign in");
		signInButton.setPrefWidth(200);

		Label registerLabel = new Label("Not registered yet?");
		registerButton = new Button("Register");
		registerButton.setPrefWidth(200);

		vbox.getChildren().addAll(
			initialsLabel,
			field,
			signInButton,
			registerLabel,
			registerButton
		);
		return vbox;
	}

	public StringProperty fieldBinding() {
		return field.textProperty();
	}

	public void onSignIn(EventHandler<ActionEvent> handler) {
		signInButton.setOnAction(handler);
	}

	public void onRegister(EventHandler<ActionEvent> handler) {
		registerButton.setOnAction(handler);
	}
}

/**
 *
 * @author Magnus
 *
 */
class SignInController implements Controller {
	@Override
	public void connected(ControllingContext context) {
		SignInView view = (SignInView) context.getView();
		Store store = context.getStore();
		view.onSignIn(event -> {
			try {
				store.signIn(view.fieldBinding().get());
				context.connect(new MenuModule());
			} catch (DoesNotExistException e) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle(e.getMessage());
				alert.setHeaderText(e.getMessage());
				alert.setContentText("Would you like to go to registration instead?");

				Optional<ButtonType> result = alert.showAndWait();
				if(result.get() == ButtonType.OK)
					context.connect(new SignUpModule());
			}
		});
		view.onRegister(event -> {
			context.connect(new SignUpModule());
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		SignInView view = (SignInView) context.getView();
		view.onSignIn(null);
	}
}