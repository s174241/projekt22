package project22.ui.helpers;

import project22.ui.Store;
/**
 *
 * @author Philip
 *
 */
public interface ControllingContext {
	public Store getStore();
	public View getView();
	public void connect(Module<View, Controller> module);
	public void connect(Module<View, Controller> module, boolean saveState);
	public void back();
}
