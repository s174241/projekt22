package project22.ui.helpers;

import javafx.scene.Node;
/**
 *
 * @author Philip
 *
 */
public interface View {
	public Node render(ReadableStore store);
}
