package project22.ui.helpers;

import javafx.scene.control.TextField;
/**
 *
 * @author Philip
 *
 */
public class NumericField extends TextField {
	public NumericField(int initialValue) {
		this.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				this.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});
	}

	public NumericField() {
		this(0);
	}

	public void setValue(int value) {
		this.setText(String.valueOf(value));
	}

	public int getValue() {
		if (this.getText().equals("")) {
			return 0;
		} else {
			return Integer.parseInt(this.getText());
		}
	}
}
