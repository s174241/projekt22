package project22.ui.helpers;

import java.util.List;

import javafx.util.Pair;
import project22.domain.Activity;
import project22.domain.TimePeriod;
import project22.domain.TimeRegistration;
import project22.domain.User;
import project22.exceptions.DoesNotExistException;

/**
 *
 * @author Magnus
 *
 */
public interface ReadableStore {
	public List<Pair<Integer, String>> getAllProjects();
	public User getCurrentUser();
	public List<Pair<Integer, String>> getUserProjects();
	public Pair<Integer, String> getCurrentProject();
	public List<Activity> getActivities();
	public Activity getActivity();
	public List<User> getDevelopers() throws DoesNotExistException;
	public List<User> getAvailableDevelopers(TimePeriod period);
	public TimePeriod getProjectTimePeriod() throws DoesNotExistException;
	public TimePeriod getActivityTimePeriod() throws DoesNotExistException;
	public int getActivityBudgetedTime() throws DoesNotExistException;
	public int getWorkedOnActivity() throws DoesNotExistException;
	public String getProjectLeader() throws DoesNotExistException;
	public List<TimeRegistration> getRegistrations();
	public List<TimeRegistration> getRegistrations(User user);
}
