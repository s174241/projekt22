package project22.ui.helpers;

import java.time.LocalDate;

import project22.domain.Activity;
import project22.domain.TimePeriod;
import project22.domain.TimeRegistration;
import project22.domain.User;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;
import project22.exceptions.UnauthorizedException;
/**
 *
 * @author Magnus
 *
 */
public interface WritableStore {
	public void signIn(String initials) throws DoesNotExistException;
	public void signUp(String initials, String role) throws AlreadyExistsException, DoesNotExistException;
	public void openProject(Integer projectId) throws DoesNotExistException;
	public void createProject(String name, LocalDate from, LocalDate to) throws UnauthorizedException, ExceededLimitException, DoesNotExistException;
	public void openActivity(Activity activity);
	public void openActivity(String activityName) throws DoesNotExistException;
	public void setProjectName(String name) throws DoesNotExistException, UnauthorizedException;
	public void setProjectTimePeriod(TimePeriod period) throws DoesNotExistException, UnauthorizedException;
	public void createActivity(String activityName) throws UnauthorizedException, DoesNotExistException, AlreadyExistsException;
	public void setActivityTimePeriod(TimePeriod period) throws UnauthorizedException, DoesNotExistException;
	public void setActivityName(String name) throws UnauthorizedException, DoesNotExistException;
	public void setActivityBudgetedTime(int budgetedTime) throws UnauthorizedException, DoesNotExistException;
	public void addDeveloper(String initials) throws UnauthorizedException, DoesNotExistException;
	public void removeDeveloper(String initials) throws DoesNotExistException, UnauthorizedException;
	public void setProjectLeader(String initials) throws DoesNotExistException, UnauthorizedException;
	public void removeRegistration(User user, TimeRegistration reg);
	public void registerRegistration(Activity activity, TimeRegistration reg) throws DoesNotExistException, AlreadyExistsException, ExceededLimitException;
}
