package project22.ui.helpers;
/**
 *
 * @author Philip
 *
 */
public interface Controller {
	public void connected(ControllingContext context);
	public void disconnected(ControllingContext context);
}
