package project22.ui.helpers;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
/**
 *
 * @author Magnus
 *
 */
public class AlertHelper {

	public static void alertException(Exception e){
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(e.getMessage());
		alert.setHeaderText(null);
		alert.setContentText(e.getMessage());

		alert.show();
	}

	public static String showStringAlert(String message){
		TextInputDialog dialog = new TextInputDialog("");
		dialog.setTitle(message);
		dialog.setHeaderText(null);
		dialog.setContentText(message);

		Optional<String> result = dialog.showAndWait();
		if(result.isPresent())
			return result.get();
		return "";
	}


}
