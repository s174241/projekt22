package project22.ui.helpers;

/**
 *
 * @author Philip
 *
 */
public interface Module<V extends View, C extends Controller> {
	public V getView();
	public C getController();
}
