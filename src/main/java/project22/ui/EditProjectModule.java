package project22.ui;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.Pair;
import project22.domain.Activity;
import project22.domain.TimePeriod;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.UnauthorizedException;
import project22.ui.helpers.AlertHelper;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;

public class EditProjectModule implements Module<View, Controller> {
	private View view;
	private Controller controller;

	public EditProjectModule() {
		this.view = new EditProjectView();
		this.controller = new EditProjectController();
	}

	@Override
	public View getView() {
		return view;
	}

	@Override
	public Controller getController() {
		return controller;
	}
}
/**
 *
 * @author Philip
 *
 */
class EditProjectView implements View {
	private TextField nameField;
	private DatePicker fromPicker;
	private DatePicker toPicker;
	private Button addButton;
	private Button saveButton;
	private TextField projectLeaderField;
	private Map<Button, Activity> activities = new HashMap<>();

	public Node render(ReadableStore store) {
		Pair<Integer, String> project = store.getCurrentProject();

		VBox vbox = new VBox(10);

		Label nameLabel = new Label("Name (" + project.getKey() + ")");
		nameLabel.setFont(new Font("Roboto", 20));
		nameField = new TextField(project.getValue());

		Label periodLabel = new Label("Time period");
		periodLabel.setFont(new Font("Roboto", 20));
		HBox dateBox = new HBox(10);
		Label between = new Label("to");
		fromPicker = new DatePicker();
		toPicker = new DatePicker();
		try {
			TimePeriod period = store.getProjectTimePeriod();
			if(period != null){
				fromPicker.setValue(period.getStartDate().toLocalDate());
				toPicker.setValue(period.getEndDate().toLocalDate());
			}
		} catch (DoesNotExistException e) {
			e.printStackTrace();
		}
		dateBox.getChildren().addAll(fromPicker, between, toPicker);

		HBox projectLeader = new HBox(10);
		Label projectLeaderText = new Label("Current project leader: ");
		projectLeaderText.setFont(new Font("Roboto",13));
		projectLeaderField = new TextField("");
		try {
			String leader = store.getProjectLeader();
			if(projectLeader != null)
				projectLeaderField.setText(leader);
		} catch (DoesNotExistException e) {}
		projectLeader.getChildren().addAll(projectLeaderText,projectLeaderField);

		Label listLabel = new Label("Activities");
		listLabel.setFont(new Font("Roboto", 20));
		VBox list = createList(store.getActivities());

		addButton = new Button("Add activity");
		saveButton = new Button("Save");

		vbox.getChildren().addAll(
			nameLabel,
			nameField,
			periodLabel,
			dateBox,
			projectLeader,
			listLabel,
			list,
			addButton,
			saveButton
		);
		return vbox;
	}

	private VBox createListItem(Activity activity) {
		Label name = new Label(activity.getName());
		name.setFont(new Font("Roboto", 16));
		Button button = new Button("View");
		activities.put(button, activity);
		button.setPrefWidth(200);
		VBox box = new VBox(10);
		box.setPadding(new Insets(10, 10, 10, 10));
		box.setStyle("-fx-background-color: #dddddd");
		box.getChildren().addAll(name, button);
		return box;
	}

	private VBox createList(List<Activity> activities) {
		VBox box = new VBox(10);
		for (Activity activity: activities) {
			VBox node = createListItem(activity);
			box.getChildren().add(node);
		}
		return box;
	}

	public String getProjectLeaderName() {
		return this.projectLeaderField.getText();
	}

	public String getName() {
		return this.nameField.getText();
	}

	public LocalDate getFromDate() {
		return this.fromPicker.getValue();
	}

	public LocalDate getToDate() {
		return this.toPicker.getValue();
	}

	public void bindButtons(Consumer<Activity> handler) {
		Set<Button> buttons = activities.keySet();
		for (Button button: buttons) {
			Activity activity = activities.get(button);
			button.setOnAction(ev -> {
				handler.accept(activity);
			});
		}
	}

	public void onAdd(Runnable handler) {
		addButton.setOnAction(ev ->{
			handler.run();
		});
	}

	public void onSave(Runnable handler) {
		saveButton.setOnAction(ev -> {
			handler.run();
		});
	}
}

/**
 *
 * @author Magnus
 *
 */
class EditProjectController implements Controller {
	@Override
	public void connected(ControllingContext context) {
		EditProjectView view = (EditProjectView) context.getView();
		Store store = context.getStore();

		String originalName = store.getCurrentProject().getValue();
		TimePeriod checkPeriod = null;
		try {
			checkPeriod = store.getProjectTimePeriod();
		} catch (DoesNotExistException e) {}

		TimePeriod originalPeriod = checkPeriod;

		String pl = "";
		try {
			pl = store.getProjectLeader();
		} catch (DoesNotExistException e1) {}
		String originalProjectLeader = pl;

		view.bindButtons(activity -> {
			store.openActivity(activity);
			context.connect(new EditActivityModule());
		});

		view.onAdd(() -> {
			context.connect(new CreateActivityModule());
		});

		view.onSave(() -> {
			String name = view.getName();
			LocalDate from = view.getFromDate();
			LocalDate to = view.getToDate();

			if (!originalName.equals(name)) {
				try {
					store.setProjectName(name);
				} catch (DoesNotExistException | UnauthorizedException e) {
					AlertHelper.alertException(e);
				}
			}

			if (from != null && to != null) {
				TimePeriod period = new TimePeriod(from,to);
				if (originalPeriod == null || !originalPeriod.equals(period)) {
					try {
						store.setProjectTimePeriod(period);
					} catch (DoesNotExistException | UnauthorizedException e) {
						AlertHelper.alertException(e);
					}
				}
			}

			String projectLeader = view.getProjectLeaderName();
			if ((originalProjectLeader == null && !projectLeader.equals(""))
					|| (originalProjectLeader != null && !originalProjectLeader.equals("") && projectLeader.equals(""))
					|| (originalProjectLeader != null && !projectLeader.equals(originalProjectLeader))){
				try {
					store.setProjectLeader(projectLeader);
				} catch (DoesNotExistException | UnauthorizedException e) {
					AlertHelper.alertException(e);
				}
			}
			context.connect(new EditProjectModule(),false);
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		EditProjectView view = (EditProjectView) context.getView();
		view.bindButtons(null);
		view.onAdd(null);
		view.onSave(null);
	}
}