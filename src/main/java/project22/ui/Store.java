package project22.ui;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

import javafx.util.Pair;
import project22.application.Application;
import project22.domain.Activity;
import project22.domain.TimePeriod;
import project22.domain.TimeRegistration;
import project22.domain.User;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;
import project22.exceptions.UnauthorizedException;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.WritableStore;
/**
 *
 * @author Magnus
 *
 */
public class Store implements ReadableStore, WritableStore {
	private Application app;
	private Activity activity;

	public Store(Application application) {
		this.app = application;

		try {
			this.signUp("dev1", "developer");
			this.signUp("dev2", "developer");
			this.signUp("dev3", "developer");
			this.signUp("dev4", "developer");
			this.signUp("pl1", "project leader");
			this.signUp("pl2", "project leader");
			this.signUp("pl3", "project leader");
			this.signIn("pl1");
			this.createProject("Project 1", LocalDate.of(2018, 5, 1), LocalDate.of(2018, 5, 7));
			this.openProject(180000);
			this.createActivity("Activity 1");
			this.openActivity("Activity 1");
			this.setActivityTimePeriod(new TimePeriod("16-03-2018 08:30:00","17-05-2018 08:30:00"));
		} catch (UnauthorizedException | ExceededLimitException | DoesNotExistException e) {
			e.printStackTrace();
		} catch (AlreadyExistsException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void signIn(String initials) throws DoesNotExistException {
		app.appUsers.setCurrentUser(initials);
	}

	@Override
	public void signUp(String initials, String role) throws AlreadyExistsException, DoesNotExistException {
		if (role.equalsIgnoreCase("project leader"))
			app.appUsers.registerProjectLeader(initials);
		else if (role.equalsIgnoreCase("developer"))
			app.appUsers.registerDeveloper(initials);
		else
			throw new DoesNotExistException("That role does not exist");
	}

	@Override
	public List<User> getAvailableDevelopers(TimePeriod period) {
		return app.appInfo.company.getAvailableDevelopers(period);
	}

	@Override
	public List<Pair<Integer, String>> getAllProjects() {
		return app.appInfo.company.getProjects().stream()
				.map(p -> new Pair<Integer, String>(p.getId(), p.getName()))
				.collect(Collectors.toList());
	}

	@Override
	public void openProject(Integer projectId) throws DoesNotExistException {
		app.appProjects.openProject(projectId);
	}

	@Override
	public User getCurrentUser() {
		return app.appInfo.getCurrentUser();
	}

	@Override
	public void createProject(String name, LocalDate from, LocalDate to)
			throws UnauthorizedException, ExceededLimitException, DoesNotExistException {
		int id = app.appProjects.createProject();
		app.appProjects.openProject(id);
		if (name != null)
			app.appProjects.setProjectName(name);
		if (from != null && to != null) {
			TimePeriod period = new TimePeriod(LocalDateTime.of(from, LocalTime.MIN),
					LocalDateTime.of(to, LocalTime.MAX));
			app.appProjects.setProjectTimePeriod(period);
		}
	}

	@Override
	public List<Pair<Integer, String>> getUserProjects() {
		return app.appInfo.company.getProjects().stream()
				.filter(p -> p.findDeveleopersOfProject().contains(app.appInfo.currentUser))
				.map(p -> new Pair<Integer, String>(p.getId(), p.getName()))
				.collect(Collectors.toList());
	}

	@Override
	public Pair<Integer, String> getCurrentProject() {
		return new Pair<Integer, String>(app.appInfo.openProject.getId(), app.appInfo.openProject.getName());
	}

	@Override
	public List<Activity> getActivities() {
		return app.appInfo.openProject.activities;
	}

	@Override
	public void openActivity(Activity activity) {
		this.activity = activity;
	}

	public void openActivity(String activity) throws DoesNotExistException {
		this.activity = app.appInfo.getActivity(activity);
	}

	@Override
	public Activity getActivity() {
		return activity;
	}

	@Override
	public List<User> getDevelopers() throws DoesNotExistException {
		return activity.getUsers();
	}

	@Override
	public TimePeriod getProjectTimePeriod() throws DoesNotExistException {
		return app.appProjects.getTimePeriod();
	}

	@Override
	public void setProjectName(String name) throws DoesNotExistException, UnauthorizedException {
		app.appProjects.setProjectName(name);

	}

	@Override
	public void setProjectTimePeriod(TimePeriod period) throws DoesNotExistException, UnauthorizedException {
		app.appProjects.setProjectTimePeriod(period);
	}

	@Override
	public void createActivity(String activityName)
			throws UnauthorizedException, DoesNotExistException, AlreadyExistsException {
		app.appActivity.createActivity(activityName);
	}

	@Override
	public TimePeriod getActivityTimePeriod() throws DoesNotExistException {
		return activity.getTimePeriod();
	}

	@Override
	public void setActivityTimePeriod(TimePeriod period) throws UnauthorizedException, DoesNotExistException {
		app.appActivity.setActivityTimePeriod(activity.getName(), period);
	}

	@Override
	public void setActivityName(String name) throws UnauthorizedException, DoesNotExistException {
		app.appActivity.setActivityName(activity.getName(), name);
	}

	@Override
	public int getActivityBudgetedTime() throws DoesNotExistException {
		return activity.getBudgetedTime();
	}

	@Override
	public int getWorkedOnActivity() throws DoesNotExistException {
		return activity.getHoursWorkedOn();
	}

	@Override
	public void setActivityBudgetedTime(int budgetedTime) throws UnauthorizedException, DoesNotExistException {
		app.appActivity.setActivityBudgetTime(activity.getName(), budgetedTime);
	}

	@Override
	public void addDeveloper(String initials) throws UnauthorizedException, DoesNotExistException {
		app.appActivity.addUserToActivity(activity, initials);
	}

	@Override
	public void removeDeveloper(String initials) throws DoesNotExistException, UnauthorizedException {
		app.appActivity.removeUserFromActivity(activity, initials);

	}

	@Override
	public String getProjectLeader() throws DoesNotExistException {
		return app.appProjects.getProjectLeader();
	}

	@Override
	public void setProjectLeader(String initials) throws DoesNotExistException, UnauthorizedException {
		app.appProjects.assignProjectLeader(initials);
	}

	@Override
	public List<TimeRegistration> getRegistrations() {
		User user = getCurrentUser();
		return getRegistrations(user);
	}

	@Override
	public List<TimeRegistration> getRegistrations(User user) {
		return user
				.getActivities()
				.stream()
				.flatMap(activity -> activity.getRegistry().stream())
				.filter(reg -> reg.getWorker().equals(user))
				.collect(Collectors.toList());
	}

	@Override
	public void removeRegistration(User user, TimeRegistration reg) {
		for (Activity activity: user.getActivities()) {
			List<TimeRegistration> registry = activity.getRegistry();
			for (TimeRegistration registration: registry) {
				if (reg.equals(registration)) {
					registry.remove(reg);
					break;
				}
			}
		}
	}

	@Override
	public void registerRegistration(Activity activity2, TimeRegistration reg) throws DoesNotExistException, AlreadyExistsException, ExceededLimitException {
		app.appTimeRegistry.registerHoursOnActivity(activity.getName(), reg.getWorkDate(), reg.getTimeSpent());
	}
}
