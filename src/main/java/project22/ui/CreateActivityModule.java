package project22.ui;

import java.time.LocalDate;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import project22.domain.TimePeriod;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.UnauthorizedException;
import project22.ui.helpers.AlertHelper;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;
import project22.ui.helpers.WritableStore;

public class CreateActivityModule implements Module<View, Controller> {
	private View view;
	private Controller controller;

	public CreateActivityModule() {
		this.view = new CreateActivityView();
		this.controller = new CreateActivityController();
	}

	@Override
	public View getView() {
		return this.view;
	}

	@Override
	public Controller getController() {
		return this.controller;
	}
}
/**
 *
 * @author Philip
 *
 */
class CreateActivityView implements View {
	private TextField nameField;
	private DatePicker fromPicker;
	private DatePicker toPicker;
	private Button saveButton;

	public Node render(ReadableStore store) {
		VBox vbox = new VBox(10);

		Label nameLabel = new Label("Name");
		nameLabel.setFont(new Font("Roboto", 20));
		nameField = new TextField();

		Label periodLabel = new Label("Time period (optional)");
		periodLabel.setFont(new Font("Roboto", 20));
		HBox dateBox = new HBox(10);
		Label between = new Label("to");
		fromPicker = new DatePicker();
		toPicker = new DatePicker();
		dateBox.getChildren().addAll(fromPicker, between, toPicker);
		saveButton = new Button("Create");

		vbox.getChildren().addAll(
			nameLabel,
			nameField,
			periodLabel,
			dateBox,
			saveButton
		);
		return vbox;
	}

	public String getName() {
		return this.nameField.getText();
	}

	public LocalDate getFromDate() {
		return this.fromPicker.getValue();
	}

	public LocalDate getToDate() {
		return this.toPicker.getValue();
	}

	public void onSave(Runnable handler) {
		saveButton.setOnAction(ev -> {
			handler.run();
		});
	}
}
/**
 *
 * @author Magnus
 *
 */
class CreateActivityController implements Controller {
	@Override
	public void connected(ControllingContext context) {
		CreateActivityView view = (CreateActivityView) context.getView();
		WritableStore store = context.getStore();

		view.onSave(() -> {
			String name = view.getName();
			LocalDate from = view.getFromDate();
			LocalDate to = view.getToDate();

			try {
				if(name == null || name.equals(""))
					throw new DoesNotExistException("Please specify a name");
				store.createActivity(name);
				store.openActivity(name);
			} catch (UnauthorizedException | DoesNotExistException | AlreadyExistsException e) {
				AlertHelper.alertException(e);
				return;
			}

			if (from != null && to != null) {
				TimePeriod period = new TimePeriod(
					from.atTime(8, 0),
					to.atTime(16, 0)
				);
				try {
					store.setActivityTimePeriod(period);
				} catch (UnauthorizedException | DoesNotExistException e) {
					e.printStackTrace();
				}
			}
			context.connect(new EditActivityModule(), false);
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		CreateActivityView view = (CreateActivityView) context.getView();
		view.onSave(null);
	}
}