package project22.ui;

import java.util.Stack;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.View;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
/**
 *
 * @author Philip
 *
 */
public class App extends Application implements ControllingContext {
	static final int WIDTH = 1280, HEIGHT = 720;
	private HBox root;
	private Store store;
	private Module<View, Controller> module;
	private Stack<Module<View, Controller>> history;

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage stage) {
		project22.application.Application app = new project22.application.Application();
		this.store = new Store(app);

		history = new Stack<>();
		stage.setTitle("Project Management");

		ScrollPane pane = new ScrollPane();
		pane.setPrefHeight(720);
		pane.setMaxHeight(720);
		pane.setPrefWidth(1280);
		pane.setFitToHeight(false);
		pane.setFitToWidth(true);

		root = new HBox();
		root.setAlignment(Pos.CENTER);
		root.setPadding(new Insets(20, 20, 20, 20));

		pane.setContent(root);

		Scene scene = new Scene(pane, WIDTH, HEIGHT);
		stage.setScene(scene);
		stage.show();

		scene.setOnKeyReleased((event) ->{
			if(event.getCode().toString().equals("HOME"))
				this.back();
		});

		this.connect(new SignInModule());
	}

	@Override
	public void stop() {
		System.out.println("Application stopped.");
	}


	public void connect(Module<View,Controller> incomingModule, boolean saveHistory){
		if(saveHistory){
			if(this.module != null)
				history.push(this.module);
		}

		if (this.module != null) {
			Controller controller = this.module.getController();
			controller.disconnected(this);
			//View view = this.module.getView();
			this.root.getChildren().clear();
		}

		this.module = incomingModule;
		View view = this.module.getView();
		Controller controller = this.module.getController();

		Node node = view.render(this.store);
		this.root.getChildren().add(node);
		controller.connected(this);
	}

	public void connect(Module<View,Controller> incomingModule) {
		connect(incomingModule,true);
	}

	public void back() {
		if(!history.isEmpty())
			this.connect(this.history.pop(),false);
	}

	@Override
	public Store getStore() {
		return this.store;
	}

	@Override
	public View getView() {
		return this.module.getView();
	}
}
