package project22.ui;


import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import project22.domain.Activity;
import project22.domain.SpecialActivity;
import project22.domain.TimePeriod;
import project22.domain.TimeRegistration;
import project22.domain.User;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;
import project22.ui.helpers.AlertHelper;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.NumericField;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;
/**
 *
 * @author Philip
 *
 */
public class RegisterHoursModule implements Module<View, Controller> {
	private View view;
	private Controller controller;

	public RegisterHoursModule() {
		this.view = new RegisterHoursView();
		this.controller = new RegisterHoursController();
	}

	@Override
	public View getView() {
		return view;
	}

	@Override
	public Controller getController() {
		return controller;
	}
}

class RegisterHoursView implements View {
	private RadioButton sickLeaveRb, vacationRb, courseRb, otherRb;
	private DatePicker datePicker;
	private NumericField fromHours, fromMinutes;
	private NumericField toHours, toMinutes;
	private Button registerButton;
	private Activity selectedActivity;

	public Node render(ReadableStore store) {
		VBox vbox = new VBox(10);

		Label groupLabel = new Label("Type");
		ToggleGroup group = new ToggleGroup();
		sickLeaveRb = new RadioButton("Sick leave");
		sickLeaveRb.setToggleGroup(group);
		vacationRb = new RadioButton("Vacation");
		vacationRb.setToggleGroup(group);
		courseRb = new RadioButton("Course");
		courseRb.setToggleGroup(group);
		otherRb = new RadioButton("Other");
		otherRb.setToggleGroup(group);

		VBox activitiesBox = new VBox();
		List<Activity> activities = store
				.getCurrentUser()
				.getActivities()
				.stream()
				.filter(activity -> !(activity instanceof SpecialActivity))
				.collect(Collectors.toList());
		VBox activityList = createList(activities);
		otherRb.selectedProperty().addListener(ev -> {
			if (otherRb.isSelected() && !activitiesBox.getChildren().contains(activityList)) {
				activitiesBox.getChildren().add(activityList);
			}
		});

		Label dateLabel = new Label("Date");
		datePicker = new DatePicker();

		VBox timeBox = new VBox(10);
		Label timeLabel = new Label("Time");
		timeLabel.setFont(new Font("Roboto", 20));

		VBox fromBox = new VBox(10);
		Label fromLabel = new Label("From");
		fromHours = new NumericField();
		fromMinutes = new NumericField();
		fromBox.getChildren().addAll(fromLabel, fromHours, fromMinutes);

		VBox toBox = new VBox(10);
		Label toLabel = new Label("To");
		toHours = new NumericField();
		toMinutes = new NumericField();
		toBox.getChildren().addAll(toLabel, toHours, toMinutes);

		timeBox.getChildren().addAll(fromBox, toBox);

		registerButton = new Button("Register");

		vbox.getChildren().addAll(
			groupLabel,
			sickLeaveRb,
			vacationRb,
			courseRb,
			otherRb,
			activitiesBox,
			dateLabel,
			datePicker,
			timeLabel,
			timeBox,
			registerButton
		);
		return vbox;
	}

	private VBox createListItem(Activity activity) {
		Label name = new Label(activity.getName());
		name.setFont(new Font("Roboto", 16));
		Button button = new Button("Select");
		button.setOnAction(ev -> {
			this.selectedActivity = activity;
		});
		button.setPrefWidth(200);
		VBox box = new VBox(10);
		box.setPadding(new Insets(10, 10, 10, 10));
		box.setStyle("-fx-background-color: #dddddd");
		box.getChildren().addAll(name, button);
		return box;
	}

	private VBox createList(List<Activity> activities) {
		VBox box = new VBox(10);
		for (Activity activity: activities) {
			VBox node = createListItem(activity);
			box.getChildren().add(node);
		}
		return box;
	}

	private LocalDateTime getFromDate() {
		return datePicker.getValue().atTime(
			this.fromHours.getValue(),
			this.fromMinutes.getValue()
		);
	}

	private LocalDateTime getToDate() {
		return datePicker.getValue().atTime(
			this.toHours.getValue(),
			this.toMinutes.getValue()
		);
	}

	public TimePeriod getTimePeriod() {
		return new TimePeriod(this.getFromDate(), this.getToDate());
	}

	public Activity getSelectedActivity() {
		TimePeriod period = this.getTimePeriod();
		if (this.sickLeaveRb.isSelected()) {
			return new SpecialActivity(SpecialActivity.Type.SICK_LEAVE, period);
		} else if (this.vacationRb.isSelected()) {
			return new SpecialActivity(SpecialActivity.Type.VACATION, period);
		} else if (this.courseRb.isSelected()) {
			return new SpecialActivity(SpecialActivity.Type.WORK_RELATED, period);
		}
		else {
			return this.selectedActivity;
		}
	}

	public boolean isSpecialActivity() {
		return !this.otherRb.isSelected();
	}

	public void onRegister(Runnable handler) {
		this.registerButton.setOnAction(ev -> {
			handler.run();
		});
	}
}

class RegisterHoursController implements Controller {
	@Override
	public void connected(ControllingContext context) {
		RegisterHoursView view = (RegisterHoursView) context.getView();
		Store store = context.getStore();

		view.onRegister(() -> {
			TimePeriod period = view.getTimePeriod();
			User user = store.getCurrentUser();
			Activity activity = view.getSelectedActivity();
			if (view.isSpecialActivity()) {
				activity.addUser(user);
			}
			
			TimeRegistration registration = new TimeRegistration(period, user);
			try {
				store.registerRegistration(activity,registration);
				context.connect(new MenuModule(), false);
			} catch (DoesNotExistException | AlreadyExistsException | ExceededLimitException e) {
				AlertHelper.alertException(e);
			}
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		RegisterHoursView view = (RegisterHoursView) context.getView();
		view.onRegister(null);
	}
}
