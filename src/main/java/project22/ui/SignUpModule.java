package project22.ui;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.ui.helpers.AlertHelper;
import project22.ui.helpers.Controller;
import project22.ui.helpers.ControllingContext;
import project22.ui.helpers.Module;
import project22.ui.helpers.ReadableStore;
import project22.ui.helpers.View;
import project22.ui.helpers.WritableStore;

public class SignUpModule implements Module<View, Controller> {
	private View view;
	private Controller controller;

	public SignUpModule() {
		this.view = new SignUpView();
		this.controller = new SignUpController();
	}

	@Override
	public View getView() {
		return view;
	}

	@Override
	public Controller getController() {
		return controller;
	}
}
/**
 *
 * @author Philip
 *
 */
class SignUpView implements View {
	TextField initials;
	RadioButton devRadio;
	RadioButton leaderRadio;
	Button signUpButton;

	public Node render(ReadableStore store) {
		VBox vbox = new VBox(10);
		vbox.setAlignment(Pos.CENTER_LEFT);
		vbox.setPrefHeight(App.HEIGHT - 40);

		Label initialsLabel = new Label("Your initials");
		initials = new TextField();

		Label roleLabel = new Label("Your role");
		ToggleGroup roleGroup = new ToggleGroup();
		devRadio = new RadioButton("Developer");
		devRadio.setToggleGroup(roleGroup);
		leaderRadio = new RadioButton("Project leader");
		leaderRadio.setToggleGroup(roleGroup);
		roleGroup.selectToggle(devRadio);

		devRadio.setSelected(true);

		signUpButton = new Button("Register");

		vbox.getChildren().addAll(initialsLabel, initials, roleLabel, devRadio, leaderRadio, signUpButton);
		return vbox;
	}

	public String getInitials() {
		return this.initials.getText();
	}

	public String getRole() {
		return leaderRadio.isSelected() ? "project leader" : "developer";
	}

	public void onSignUp(Runnable handler) {
		this.signUpButton.setOnAction(ev -> {
			handler.run();
		});
	}
}
/**
 *
 * @author Magnus
 *
 */
class SignUpController implements Controller {

	@Override
	public void connected(ControllingContext context) {
		SignUpView view = (SignUpView) context.getView();
		WritableStore store = context.getStore();

		view.onSignUp(() -> {
			String initials = view.getInitials();
			String role = view.getRole();

			try {
				store.signUp(initials, role);
			} catch (AlreadyExistsException | DoesNotExistException e) {
				AlertHelper.alertException(e);
				return;
			}

			try {
				store.signIn(initials);
				context.connect(new MenuModule(), false);
			} catch (DoesNotExistException e) {
				context.back();
			}
		});
	}

	@Override
	public void disconnected(ControllingContext context) {
		SignUpView view = (SignUpView) context.getView();
		view.onSignUp(null);
	}
}