package project22.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import project22.application.Application;
import project22.domain.Activity;
import project22.domain.TimePeriod;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;
import project22.exceptions.UnauthorizedException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
/**
 *
 * @author Adam
 *
 */
public class ProjectSteps {
	Application application;
	InfoHolder info;

	public ProjectSteps(Application application, InfoHolder info) {
		this.application = application;
		this.info = info;
	}

	@When("^The user creates a project with name \"([^\"]*)\"$")
	public void the_user_creates_a_project_with_name(String name) {
		try {
			info.projectId = application.appProjects.createProject();
			application.appInfo.openProject(info.projectId);
			application.appProjects.setProjectName(name);
		} catch (DoesNotExistException | UnauthorizedException | ExceededLimitException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@Then("^A project exists with name \"([^\"]*)\"$")
	public void a_project_exists_with_name(String name) throws Exception {
		assertTrue(application.appProjects.getProjectNames().contains(name));
	}

	@Given("^A project exists$")
	public void a_project_exists() throws Exception {
		info.projectId = application.appProjects.adminCreateProject();
		application.appInfo.openProject(info.projectId);
	}

	@Given("^Another project exists$")
	public void anotherProjectExists() throws Exception {
		info.secondProjectId = application.appProjects.adminCreateProject();
	}

	@When("^The user changes the name of the project to \"([^\"]*)\"$")
	public void the_user_changes_the_name_of_the_project_to(String name) {

		try {
			application.appProjects.setProjectName(name);
		} catch (DoesNotExistException | UnauthorizedException e) {
			// TODO Auto-generated catch block
			info.errorMessage = e.getMessage();
		}
	}

	@Given("^The project does not exist$")
	public void the_project_does_not_exist() throws Exception {
		application.appInfo.openProject = null;
	}

	@Then("^The projects budget time is (\\d+) weeks$")
	public void theProjectsBudgetTimeIsWeeks(int weeks) throws Exception {
		assertEquals(application.appProjects.getBudgetTime(), ActivitySteps.weeksToHours(weeks));
	}

	@When("^The user sets the time period of the project to be from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void userSetsTimePeriod(String start, String end) throws Exception {
		TimePeriod period = new TimePeriod(start, end);
		application.appProjects.setProjectTimePeriod(period);
	}

	@Then("^The projects time period is from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void ensureTimePeriod(String start, String end) throws Exception {
		TimePeriod period = new TimePeriod(start, end);
		assertEquals(application.appProjects.getTimePeriod(),period);
	}

	@Then("^The list of developers on the first project contains \\(at least\\) \"([^\"]*)\" and \"([^\"]*)\"$")
	public void theListOfDevelopersOnTheFirstProjectContainsAtLeastAnd(String user1Initials, String user2Initials)
			throws Exception {
		List<String> usersOfProject = application.appProjects.getWorkersOfProject();
		assertTrue(usersOfProject.contains(user1Initials));
		assertTrue(usersOfProject.contains(user2Initials));
	}

	@Then("^The names on the list of developers on the first project contains \"([^\"]*)\" and \"([^\"]*)\" and names only show up once each$")
	public void theNamesOnTheListOfDevelopersOnTheFirstProjectContainsAndAndNamesOnlyShowUpOnceEach(
			String user1Initials, String user2Initials) throws Exception {
		List<String> usersOfProject = application.appProjects.getWorkersOfProject();
		assertTrue(usersOfProject.contains(user1Initials));
		assertTrue(usersOfProject.contains(user2Initials));
		assertEquals(usersOfProject.size(),usersOfProject.stream().collect(Collectors.toSet()).size());
	}

	@Then("^The list of developers on activity \"([^\"]*)\" contains \\(at least\\) \"([^\"]*)\" and \"([^\"]*)\"$")
	public void theListOfDevelopersOnActivityContainsAtLeastAnd(String activityName, String user1Initials,
			String user2Initials) throws Exception {
		Activity activity = application.appInfo.getActivity(activityName);
		List<String> workersOfActivity = application.appActivity.getWorkersOfActivity(activity);
		assertTrue(workersOfActivity.contains(user1Initials));
		assertTrue(workersOfActivity.contains(user2Initials));
	}

	@Then("^The list of activities developer \"([^\"]*)\" is working on contains \"([^\"]*)\" from the first project and \"([^\"]*)\" from the second project$")
	public void theListOfActivitiesDeveloperIsWorkingOnContainsFromTheFirstProjectAndFromTheSecondProject(
			String initials, String activity1String, String activity2String) throws Exception {
		List<String> activitiesOfWorker = application.appUsers.getActivitiesOfWorker(initials);
		assertTrue(activitiesOfWorker.contains(activity1String));
		assertTrue(activitiesOfWorker.contains(activity2String));
	}

}
