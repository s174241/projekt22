package project22.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import project22.application.Application;
import project22.domain.SpecialActivity;
import project22.domain.TimePeriod;
import project22.exceptions.DoesNotExistException;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
/**
 *
 * @author Philip
 *
 */
public class SpecialActivitySteps {
	private Application application;
	InfoHolder info;
	public SpecialActivitySteps(Application application, InfoHolder info) {
		this.application = application;
		this.info = info;
	}

	@When("^The developer registers a \"([^\"]*)\" from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void registersVacation(String typeText, String start, String end){
		TimePeriod timePeriod = new TimePeriod(start, end);
		application.appTimeRegistry.registerSpecialActivity(typeText,timePeriod);
	}

	@When("^The developer edits the special registration from \"([^\"]*)\" to \"([^\"]*)\" to be from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void theDeveloperEditsTheSpecialRegistrationFromToToBeFromTo(String start1, String end1, String start2, String end2) {
	    TimePeriod from = new TimePeriod(start1,end1);
	    TimePeriod to = new TimePeriod(start2,end2);
	    application.appTimeRegistry.editSpecialActivity(from,to);
	}

	@Then("The developer has registered a (vacation|sick day|course) from \"([^\"]*)\" to \"([^\"]*)\"")
	public void ensureRegistration(String type, String start, String end) throws DoesNotExistException {
		TimePeriod period = new TimePeriod(start, end);
		SpecialActivity activity = (SpecialActivity) application.appTimeRegistry.getSpecialActivityInPeriod(period);
		assertNotNull(activity);
		if(type.equals("vacation"))
			assertEquals(SpecialActivity.Type.VACATION,activity.getType());
		else if(type.equals("sick day"))
			assertEquals(SpecialActivity.Type.SICK_LEAVE,activity.getType());
		else //(type.equals("course"))
			assertEquals(SpecialActivity.Type.WORK_RELATED,activity.getType());
	}

	@And("The developer is unavailable in the period \"([^\"]*)\" to \"([^\"]*)\"")
	public void ensureUnavailability(String start, String end) throws DoesNotExistException {
		TimePeriod period = new TimePeriod(start, end);
		assertFalse(application.appTimeRegistry.isUserAvailable(application.appInfo.getCurrentUser(), period));
	}

	@Then("^The developer is available in the period \"([^\"]*)\" to \"([^\"]*)\"$")
	public void theDeveloperIsAvailableInThePeriodTo(String start, String end) throws Exception {
	    TimePeriod period = new TimePeriod(start, end);
	    assertTrue(application.appTimeRegistry.isUserAvailable(application.appInfo.getCurrentUser(), period));
	}
}
