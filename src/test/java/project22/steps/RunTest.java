package project22.steps;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
/**
 *
 * @author Magnus
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "use_cases",
	plugin = { "html:target/cucumber/wikipedia.html"},
	monochrome=true,
	snippets = SnippetType.CAMELCASE,
	glue = { "project22"})
public class RunTest {}