package project22.steps;
/**
 *
 * @author Adam
 *
 */
public class InfoHolder {

	public static final String USER_INITIALS = "R2D2";

	public String errorMessage;
	public int projectId;
	public int secondProjectId;

	public InfoHolder() {
		errorMessage  = "";
		projectId = 0;
		secondProjectId = 0;
	}

}
