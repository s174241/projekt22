package project22.steps;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import project22.application.Application;
import project22.domain.TimePeriod;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author Magnus
 *
 */
public class TimeRegistrationSteps {

	static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	private static LocalDateTime toDate(String string) {
		return LocalDateTime.parse(string, formatter);
	}

	InfoHolder info;
	Application application;

	public TimeRegistrationSteps(InfoHolder info, Application application) {
		this.info=info;
		this.application = application;
	}

	@When("^The user registers (\\d+) hours on the project on activity \"([^\"]*)\" on \"([^\"]*)\"$")
	public void theUserRegistersHoursOnTheProjectOnActivityOn(int hours, String activityName, String date) {
	    try {
			application.appTimeRegistry.registerHoursOnActivity(activityName, toDate(date), hours);
		} catch (DoesNotExistException | AlreadyExistsException | ExceededLimitException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@Then("^The user has (\\d+) hours registered in the project on activity \"([^\"]*)\" on \"([^\"]*)\"$")
	public void theUserHasHoursRegisteredInTheProjectOnActivityOn(int hours, String activityName, String date) throws Exception {
	    int registeredHours =application.appTimeRegistry.getUserHoursOnActivity(activityName,toDate(date));
		assertEquals(registeredHours,hours);
	}

	@Then("^The project has (\\d+) hours registered in period \"([^\"]*)\" to \"([^\"]*)\"$")
	public void theProjectHasHoursRegisteredInPeriodTo(int hours, String start, String end) throws Exception {
	    TimePeriod period = new TimePeriod(start,end);
	    assertEquals(hours,application.appInfo.getHoursWorked(period));
	}

}
