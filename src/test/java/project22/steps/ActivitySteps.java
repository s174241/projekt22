package project22.steps;

import static org.junit.Assert.assertFalse;

import java.util.List;

import static org.junit.Assert.assertEquals;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import project22.application.Application;
import project22.domain.Activity;
import project22.domain.TimePeriod;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.UnauthorizedException;
/**
 *
 * @author Magnus
 *
 */
public class ActivitySteps {

	Application application;
	InfoHolder info;

	public ActivitySteps(Application application, InfoHolder info) {
		this.application = application;
		this.info = info;
	}

	protected static int weeksToHours(int weeks) {
		return 37 * 2 * weeks;
	}

	@When("^The user creates activity \"([^\"]*)\" in the project with expected work (\\d+) weeks$")
	public void theUserCreatesActivityInTheProjectWithExpectedWorkWeeks(String activityName, int workLoadInWeeks) {
		try {
			application.appActivity.createActivity(activityName);
			application.appActivity.setActivityBudgetTime(activityName, weeksToHours(workLoadInWeeks));
		} catch (DoesNotExistException | AlreadyExistsException | UnauthorizedException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@Then("^An activity in the project has name \"([^\"]*)\" with expected work (\\d+) weeks$")
	public void anActivityInTheProjectHasNameWithExpectedWorkWeeks(String activityName, int workLoadInWeeks)
			throws DoesNotExistException {
		assertEquals(application.appActivity.getBudgetTimeOfActivity(activityName), weeksToHours(workLoadInWeeks));
	}

	@When("^The user edits activity \"([^\"]*)\" in the project to have name \"([^\"]*)\" and expected work (\\d+) weeks$")
	public void theUserEditsActivityInTheProjectToHaveNameAndExpectedWorkWeeks(String activityName, String newName,
			int newBudgetTime) {
		try {
			application.appActivity.setActivityBudgetTime(activityName, weeksToHours(newBudgetTime));
			application.appActivity.setActivityName(activityName, newName);
		} catch (UnauthorizedException | DoesNotExistException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@Given("^The project has an activity with name \"([^\"]*)\"$")
	public void theProjectHasAnActivityWithName(String activityName) throws Exception {
		application.appActivity.adminCreateActivity(activityName);
	}

	@Given("^The second project has an activity with name \"([^\"]*)\"$")
	public void theSecondProjectHasAnActivityWithName(String secondActivityName) throws Exception {
		int projectId = application.appInfo.openProject.getId();
		application.appProjects.openProject(info.secondProjectId);
		application.appActivity.adminCreateActivity(secondActivityName);
		application.appProjects.openProject(projectId);
	}

	@When("^The user removes the developer \"([^\"]*)\" from the activity \"([^\"]*)\"$")
	public void theUserRemovesTheDeveloperFromTheActivity(String initials, String activityName) {
		try {
			Activity activity = application.appInfo.getActivity(activityName);
			application.appActivity.removeUserFromActivity(activity, initials);
		} catch (DoesNotExistException | UnauthorizedException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@Then("^The developer \"([^\"]*)\" is no longer assigned to the activity \"([^\"]*)\"$")
	public void theDeveloperIsNoLongerAssignedToTheActivity(String initials, String activityName) throws Exception {
		Activity activity = application.appInfo.getActivity(activityName);
		List<String> workers = application.appActivity.getWorkersOfActivity(activity);
		assertFalse(workers.contains(initials));
	}

	@When("^The user sets the time period of the activity \"([^\"]*)\" to be from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void userSetsTimePriodOfActivity(String activityName, String start, String end) {
		TimePeriod period = new TimePeriod(start, end);

		try {
			application.appActivity.setActivityTimePeriod( activityName, period);
		} catch (UnauthorizedException | DoesNotExistException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@Then("^The projects activity \"([^\"]*)\" time period is from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void ensureActivityTimePeriod(String activityName, String start,
			String end) throws Exception {
		TimePeriod period = new TimePeriod(start, end);
		assertEquals(application.appActivity.getActivityTimePeriod(activityName),period);
	}

	@Given("^The activity \"([^\"]*)\" has a time period from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void theActivityHasATimePeriodFromTo(String activityName, String start, String end) throws Exception {
		TimePeriod period = new TimePeriod(start,end);
	    application.appActivity.adminSetActivityTimePeriod(activityName,period);
	}

	@When("^The user edits activity \"([^\"]*)\" in the project to have timeperiod \"([^\"]*)\" to \"([^\"]*)\"$")
	public void theUserEditsActivityInTheProjectToHaveTimeperiodTo(String activityName, String start, String end)  {
		TimePeriod period = new TimePeriod(start, end);
	    try {
			application.appActivity.setActivityTimePeriod(activityName, period);
		} catch (UnauthorizedException | DoesNotExistException e) {
			// TODO Auto-generated catch block
			info.errorMessage = e.getMessage();
		}
	}

	@Then("^The activity \"([^\"]*)\" has time period from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void theActivityHasTimePeriodFromTo(String activityName, String start, String end) throws Exception {
	    TimePeriod period = new TimePeriod(start, end);
	    TimePeriod activityPeriod = application.appActivity.getActivityTimePeriod(activityName);
	    assertEquals(activityPeriod, period);
	}
}
