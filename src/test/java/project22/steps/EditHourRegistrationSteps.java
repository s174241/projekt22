package project22.steps;

import cucumber.api.java.en.When;
import project22.application.Application;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;
/**
 *
 * @author Jenath
 *
 */
public class EditHourRegistrationSteps {
	Application application;
	InfoHolder info;


	public EditHourRegistrationSteps(Application application, InfoHolder info) throws AlreadyExistsException {
		this.application = application;
		this.info = info;
	}

	@When("^The developer edits the project and activity \"([^\"]*)\" on \"([^\"]*)\" to instead (\\d+) hours$")
	public void theDeveloperEditsTheProjectAndActivityOnYearMonthDayToInsteadRegisterHours(String activityName, String date, int newHours) {
		try {
			application.appTimeRegistry.editHourRegisteredOnActivity(activityName, Application.stringToLocalDateTime(date), newHours);
		} catch (DoesNotExistException | AlreadyExistsException | ExceededLimitException e) {
			info.errorMessage = e.getMessage();
		}
	}


}