package project22.steps;

import cucumber.api.java.en.Then;
import project22.application.Application;

import static org.junit.Assert.assertTrue;

/**
 *
 * @author Magnus
 *
 */
public class StandardSteps {

	@SuppressWarnings("unused")
	private Application application;
	private InfoHolder info;

	public StandardSteps(Application application, InfoHolder info) {
		this.application = application;
		this.info = info;
	}

	@Then("^The system informs the user that \"([^\"]*)\"$")
	public void theSystemInformsTheUserThat(String error) throws Exception {
	    assertTrue(info.errorMessage.equals(error));
	}
}
