package project22.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import project22.application.Application;
import project22.domain.Activity;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;
import project22.exceptions.UnauthorizedException;

import static org.junit.Assert.assertTrue;

import java.util.List;

import static org.junit.Assert.assertEquals;
/**
 *
 * @author Adam
 *
 */
public class UserSteps {

	Application application;
	InfoHolder info;

	public UserSteps(Application application, InfoHolder info) {
		this.application = application;
		this.info = info;
	}

	@Given("^The user is a project leader$")
	public void the_user_is_a_project_leader() throws Exception {
		application.appUsers.registerProjectLeader(InfoHolder.USER_INITIALS);
		application.appUsers.setCurrentUser(InfoHolder.USER_INITIALS);
	}

	@Given("^The user is a developer$")
	public void the_user_is_a_developer() throws Exception {
		application.appUsers.registerDeveloper(InfoHolder.USER_INITIALS);
		application.appUsers.setCurrentUser(InfoHolder.USER_INITIALS);
	}

	@Given("^A developer \"([^\"]*)\" exists$")
	public void aDeveloperExists(String initials) {
		try {
			application.appUsers.registerDeveloper(initials);
		} catch (AlreadyExistsException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@When("^The user adds the developer \"([^\"]*)\" to the activity \"([^\"]*)\"$")
	public void theUserAddsTheDeveloperToTheActivity(String initials, String activityName)
			throws ExceededLimitException {
		try {
			Activity activity = application.appInfo.getActivity(activityName);
			application.appActivity.addUserToActivity(activity, initials);
		} catch (DoesNotExistException | UnauthorizedException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@Given("^The user adds the developer \"([^\"]*)\" to the activity \"([^\"]*)\" on the second project$")
	public void theUserAddsTheDeveloperToTheActivityOnTheSecondProject(String initials, String activityName)
			throws Exception {
		application.appInfo.openProject(info.secondProjectId);
		Activity activity = application.appInfo.getActivity(activityName);
		application.appActivity.addUserToActivity(activity, initials);
		application.appInfo.openProject(info.projectId);
	}

	@Then("^The developer \"([^\"]*)\" is assigned to the activity \"([^\"]*)\"$")
	public void theDeveloperIsAssignedToTheActivity(String initials, String activityName) throws Exception {
		Activity activity = application.appInfo.getActivity(activityName);
		List<String> workers = application.appActivity.getWorkersOfActivity(activity);
		assertTrue(workers.contains(initials));
	}

	@When("^The user assigns \"([^\"]*)\" as project leader of the project$")
	public void theUserAssignsAsProjectLeaderOfTheProject(String initials) {
		// Write code here that turns the phrase above into concrete actions
		try {
			application.appProjects.assignProjectLeader( initials);
		} catch (DoesNotExistException | UnauthorizedException e) {
			info.errorMessage = e.getMessage();
		}
	}

	@Then("^The user is the project leader of the project$")
	public void theUserIsTheProjectLeaderOfTheProject() throws Exception{
		assertEquals(application.appProjects.getProjectLeader(), InfoHolder.USER_INITIALS);
	}

	@When("^A developer \"([^\"]*)\" is added on \"([^\"]*)\"$")
	public void aDeveloperIsAddedOn(String initials, String date) throws Exception {
	    application.appUsers.registerDeveloper(initials, Application.stringToLocalDateTime(date));
	}

	@Then("^Developer \"([^\"]*)\" has creation date \"([^\"]*)\"$")
	public void developerHasCreationDate(String initials, String date) throws Exception {
		assertEquals(Application.stringToLocalDateTime(date), application.appUsers.getUserCreationDate(initials));
	}

	@Given("^The user is working on activity \"([^\"]*)\"$")
	public void theUserIsWorkingOnActivity(String activityName) throws Exception {
		Activity activity = application.appInfo.getActivity(activityName);
		application.appActivity.adminAddUserToActivity(activity, InfoHolder.USER_INITIALS);
	}

	@Then("^The list of the devevelopers is \"([^\"]*)\"$")
	public void theListOfTheDevevelopersIs(String listOfDevs) throws Exception {
		String[] devs = listOfDevs.split(",");
		List<String> devsInCompany = application.appInfo.getAllEmployees();
		assertEquals(devs.length,devsInCompany.size());
		for(String dev : devs)
			assertTrue(devsInCompany.contains(dev));
	}

	@When("^The user looks at the project leader$")
	public void theUserLooksAtTheProjectLeader() {
	    try {
			application.appProjects.getProjectLeader();
		} catch (DoesNotExistException e) {
			info.errorMessage = e.getMessage();
		}
	}

}
