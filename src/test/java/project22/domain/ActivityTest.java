package project22.domain;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class ActivityTest {
	User regDev;
	LocalDateTime regDate;
	TimePeriod regPeriod;
	TimeRegistration reg;
	Activity activity;

	@Before
	public void setup() {
		regDev = new Developer("regDev");
		regDate = LocalDateTime.of(2018, 5, 1, 8, 0);
		regPeriod = new TimePeriod(regDate, regDate.plusHours(8));
		reg = new TimeRegistration(regPeriod, regDev);
		activity = new ProjectActivity("Activity");
		activity.addTimeRegistration(reg);
	}

	/**
	 * Matching date and user
	 */
	@Test
	public void testGetHoursWorkedOn1() {
		int res = activity.getHoursWorkedOn(regDev, regDate);
		System.out.println(res);
		assertEquals(res, 8);
	}

	/**
	 * Not matching date, matching user
	 */
	@Test
	public void testGetHoursWorkedOn2() {
		LocalDateTime checkDate = LocalDateTime.of(2018, 5, 2, 8, 0);
		int res = activity.getHoursWorkedOn(regDev, checkDate);
		assertEquals(res, 0);
	}

	/**
	 * Matching date, not matching user
	 */
	@Test
	public void testGetHoursWorkedOn3() {
		User checkDev = new Developer("Dev2");
		int res = activity.getHoursWorkedOn(checkDev, regDate);
		assertEquals(res, 0);
	}

	/**
	 * Not matching date, not matching user
	 */
	@Test
	public void testGetHoursWorkedOn4() {
		User checkDev = new Developer("Dev2");
		LocalDateTime checkDate = LocalDateTime.of(2018, 5, 2, 8, 0);
		int res = activity.getHoursWorkedOn(checkDev, checkDate);
		assertEquals(res, 0);
	}
}
