package project22.domain;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 *
 * @author Jenath
 *
 */
public class CompanyTest {

	Company company = new Company();


	//Checking if employee exists in company after registering
	@Test
	public void testRegisterEmployee() {
		User Employee1 = new Developer("Employee1");
		company.registerEmployee(Employee1);
		assertTrue(Employee1.equals(company.getEmployee(Employee1.getInitials())));

	//Checking if employee exists in company if not registered
		User Employee2 = new Developer("Employee2");
		assertFalse(Employee2.equals(company.getEmployee(Employee2.getInitials())));



	}


}
