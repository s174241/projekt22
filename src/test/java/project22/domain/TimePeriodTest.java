package project22.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
/**
 *
 * @author Adam
 *
 */
public class TimePeriodTest {

	public TimePeriodTest() {

	}
/*	!(X || Y)
 * X = t1.end BEFORE t2.start
 * Y = t1.start AFTER t2.end
 *
 *    Input X Y | RETURN
 *    --------- | -----
 *A:        T ? | FALSE
 *B:        F T | FALSE
 *C:        F F | TRUE
 */


	@Test
	public void testInputDataSetA() {
		TimePeriod start = new TimePeriod("01-01-2018 08:00:00", "10-01-2018 08:00:00");
		TimePeriod end = new TimePeriod("11-01-2018 08:00:00", "15-01-2018 08:00:00");
		assertFalse(start.overlaps(end));
	}

	@Test
	public void testInputDataSetB() {
		TimePeriod start = new TimePeriod("01-01-2018 08:00:00", "10-01-2018 08:00:00");
		TimePeriod end = new TimePeriod("01-01-2017 08:00:00", "10-01-2017 08:00:00");
		assertFalse(start.overlaps(end));
	}

	@Test
	public void testinputDataSetC() {
		TimePeriod start = new TimePeriod("01-01-2018 08:00:00", "10-01-2018 08:00:00");
		TimePeriod end = new TimePeriod("05-01-2018 08:00:00", "20-01-2018 08:00:00");
		assertTrue(start.overlaps(end));
	}
}
