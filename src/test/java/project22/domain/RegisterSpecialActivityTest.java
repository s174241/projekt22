package project22.domain;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import project22.application.Application;
import project22.exceptions.AlreadyExistsException;
import project22.exceptions.DoesNotExistException;
import project22.exceptions.ExceededLimitException;
import project22.exceptions.UnauthorizedException;
/**
 *
 * @author Magnus
 * This is whitebox test for ApplicationTimeRegistration.RegisterSpecialActivity
 */
public class RegisterSpecialActivityTest {
	Application app;
	TimePeriod period;
	SpecialActivity.Type type;

	/**
	 * This is a setup for a valid application for the test
	 */
	@Before
	public void setup() throws UnauthorizedException, DoesNotExistException, AlreadyExistsException, ExceededLimitException{
		app = new Application();
		app.appUsers.registerProjectLeader("dev1");
		app.appUsers.setCurrentUser("dev1");
		app.appProjects.openProject(app.appProjects.createProject());
		period = new TimePeriod("16-03-2018 08:30:00", "16-04-2018 20:00:00");
	}

	/**
	 * Make sure the test succeeded
	 */
	@After
	public void testSuccedeed(){
		SpecialActivity activity = (SpecialActivity) app.appTimeRegistry.getSpecialActivityInPeriod(period);
		assertNotNull(activity);
		assertTrue(activity.getType().equals(type));
		assertTrue(activity.getTimePeriod().equals(period));
	}

	/**
	 * Test on dataset A
	 */
	@Test
	public void testInputSetA() throws DoesNotExistException, AlreadyExistsException, ExceededLimitException, UnauthorizedException{
		type = SpecialActivity.Type.VACATION;
		app.appTimeRegistry.registerSpecialActivity("vacation", period);

	}

	/**
	 * Test on dataset B
	 */
	@Test
	public void testInputSetB() throws UnauthorizedException, DoesNotExistException, AlreadyExistsException, ExceededLimitException{
		type = SpecialActivity.Type.SICK_LEAVE;
		app.appTimeRegistry.registerSpecialActivity("sick day", period);
	}

	/**
	 * Test on dataset C
	 */
	@Test
	public void testInputC() throws UnauthorizedException, DoesNotExistException, AlreadyExistsException, ExceededLimitException{
		type = SpecialActivity.Type.WORK_RELATED;
		app.appTimeRegistry.registerSpecialActivity("course", period);
	}

}
